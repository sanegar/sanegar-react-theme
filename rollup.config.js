import babel from 'rollup-plugin-babel'
import commonjs from 'rollup-plugin-commonjs'
import external from 'rollup-plugin-peer-deps-external'
import postcss from 'rollup-plugin-postcss'
import resolve from 'rollup-plugin-node-resolve'
import url from 'rollup-plugin-url'
import svgr from '@svgr/rollup'

import pkg from './package.json'

export default [
  {
    input: 'src/Components/Modiran/index.js',
    output: [
      {
        file: pkg.modiranMain,
        format: 'cjs',
        sourcemap: true
      },
      {
        file: pkg.modiranModule,
        format: 'es',
        sourcemap: true
      }
    ],
    plugins: [
      external(),
      postcss({
        modules: true
      }),
      url(),
      svgr(),
      babel({
        exclude: 'node_modules/**',
        plugins: [ 'external-helpers' ]
      }),
      resolve(),
      commonjs()
    ]
  },
  {
    input: 'src/Components/Saba/index.js',
    output: [
      {
        file: pkg.sabaMain,
        format: 'cjs',
        sourcemap: true
      },
      {
        file: pkg.sabaModule,
        format: 'es',
        sourcemap: true
      }
    ],
    plugins: [
      external(),
      postcss({
        modules: true
      }),
      url(),
      svgr(),
      babel({
        exclude: 'node_modules/**',
        plugins: [ 'external-helpers' ]
      }),
      resolve(),
      commonjs()
    ]
  }
]
