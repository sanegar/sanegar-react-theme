import React, {Component} from 'react';

class NoFrame extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <React.Fragment>
                {
                    this.props.children
                }
            </React.Fragment>

        );
    }
}

export {NoFrame};
