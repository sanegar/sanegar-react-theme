import React, {Component} from 'react';
import {Lang} from './../index';

class BaseFrame extends Component{
    constructor(props){
        super(props);
    }

    render(){
        let {label, labelIcon, children, minIcon, min, className, leftSide} = this.props;
        if(className == undefined) className = "col-md-12";
        let id = "layer-"+Math.round(Math.random()*10000);
        if(!minIcon) minIcon = "glyph-icon fas fa-chevron-circle-down";
        // let id = "layer";
        return(
            <div className={"card mb-4 "+className} id="contentFrame">
                <div className="card-body">
                    <h5 className="card-title mb-4 lalezar">
                        {labelIcon?<i className={labelIcon+" label-icon"}></i>:""} {Lang('public.'+label)} {leftSide}
                        {min?<a className = {minIcon+" collapsed float-left"}
                         data-toggle="collapse" href={"#"+id} role="button"
                         aria-expanded="false" aria-controls={id}></a>:""}
                    </h5>
                    <div id={id} className="collapse show">
                        {
                            children
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export {BaseFrame};
