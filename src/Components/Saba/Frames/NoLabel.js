import React, {Component} from 'react';

class NoLabel extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        let {className} = this.props;
        if(className == undefined) className = "col-md-12";
        return(
            <div className={"card mb-4 "+className} id="contentFrame">
                <div className="card-body">
                    {
                        this.props.children
                    }
                </div>
            </div>
        );
    }
}

export {NoLabel};
