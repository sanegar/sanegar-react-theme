import React, {Component} from 'react';
import axios from 'axios';
import {Config, Lang} from './../index';
import {NotificationManager} from 'react-notifications';

export class CheckSave extends Component {
    constructor(props){
        super(props);
        this.change = this.change.bind(this);
        this.state ={
            parent : {}
        }
    }

    change(route){
        axios.get(`${Config.getHost()}${route}`)
          .then((response)=>{
              let rand = Math.random()*10000;
                  NotificationManager.success(response.data, 'تغییر وضعیت انجام شد.', 5000);
                  this.state.parent.props.history.replace(this.state.parent.props.history.location.pathname+`?${rand}`);
          })
          .catch((error)=>{
              console.log(error);
          });
    }

    render(){
        let {label, name, defaultValue, className, error, route, parent} = this.props;
        let rand = Math.ceil(Math.random()*10000);
        if(className == "") className='mb-3 col-md-2';
        this.state.parent = parent;

        let divError = [];
        if(error!=undefined){
            error.map((err, index)=>{
                divError.push(<div className='alert alert-danger' id={'error-'+rand} key={index} style={{display:"block"}}>{err}</div>);
            })
        }

        return(<div className={className}>
                <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input"
                        defaultChecked={defaultValue==1?true:false}
                        value={1}
                        name={name}
                        key={rand}
                        ref='item'
                        id={name+rand}
                        onClick = {()=>this.change(route)}
                     />
                    <label className="custom-control-label" htmlFor={name+rand}>{Lang('public.'+label)}</label>
                </div>
                {divError}
            </div>
        );
    }
}
