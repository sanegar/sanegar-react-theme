import {Config} from '../index';

const DIR = () => {
    let local = Config.getStore().getState().local;

    if(local=='en')
        return 'ltr';
    else
        return ("rtl");
}

export {DIR};
