import React, { Component } from "react";

export class Base extends Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading: true,
            components : {
                "loading1" : Loading1,
                "loading2" : Loading2
            },
            typeLoading: "loading1",
            ...props,
        }
    }

    render(){
        const {components,typeLoading} = this.state
        // const {type} = this.props
        let Component = components[typeLoading]

        return(
            <React.Fragment>
                {
                    (this.state.isLoading)?
                        <Component />
                    :
                    this.display()
                }
            </React.Fragment>
        )
    }

}

function Loading1(props){
    return(
        <div className="row dashboard-page">
            <div className="col-lg-12 m-5 mx-auto text-center ltr">
                <h1>Loading...</h1>
            </div>
        </div>
    )
}

function Loading2(props){
    return(
        <div className="row dashboard-page">
            <div className="col-lg-12 m-5 mx-auto text-center ltr">
                <h1>در حال بارگذاري ...</h1>
            </div>
        </div>
    )
}
