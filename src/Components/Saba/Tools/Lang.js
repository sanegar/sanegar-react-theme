import {Config} from '../index';
import {langs} from './langs';

const Lang = (str, items = null) => {
  let lang = Config.getStore().getState().lang;
  if(!lang || Object.keys(lang) == 0){
    lang = langs;
  }

  let param = str.split('.');
  let result = lang;
  param.forEach((item)=>{
    if(result != null && result != undefined && result != "")
      result = result[item];
    else
      result = "";
  });

  if(items != null && result != null){
    Object.keys(items).forEach((key)=>{
        result = result.replace(":"+key, items[key]);
      });
  }

  if(result != "" && result != undefined && result != null)
    return result;
  else
    return str.replace('public.', '').replace('undefined', '');
}

export default Lang;

