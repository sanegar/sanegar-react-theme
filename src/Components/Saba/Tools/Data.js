import axios from 'axios';
import {Config, Lang} from '../index';
import {NotificationManager} from 'react-notifications';
import tinymce from 'tinymce/tinymce';

class Data{
    constructor(){
        // this.url = url;
    }

    getRefValue(ref, parent){
        let {refs} = parent;
        let value = "";
        if(ref.value != undefined){
            value = refs[ref].value;
        }
        else{
            // Radio Button
            if(refs[ref].refs.item == undefined && Object.keys(refs[ref].refs).length >= 1){
                Object.keys(refs[ref].refs).forEach((item)=>{
                    if(refs[ref].refs[item].checked == true){
                        value = refs[ref].refs[item].value;
                    }
                })
            }
            else if(refs[ref].refs.item.type == "checkbox" || refs[ref].refs.item.type == "radio"){
                if(value = refs[ref].refs.item.checked)
                {
                    value = refs[ref].refs.item.value;
                }
                else{
                    value = 0;
                }
            }
            else if(refs[ref].refs.item.multiple == true)
            {
                value = window.$('#'+refs[ref].refs.item.id).val();
            }
            else if(refs[ref].refs.item.dataset.type=='editor'){
                value = refs[ref].refs.item.editor.getData();
                if(value == '<p>&nbsp;</p>') value = '';
            }
            else if(refs[ref].refs.item.dataset.type=='tinyMCE'){
                let id = refs[ref].refs.item.id;
                value = tinymce.get(id).getContent();
                if(value == '<p>&nbsp;</p>') value = '';
            }
            else if(refs[ref].refs.item.dataset.type=='currency'){
                value = refs[ref].refs.item.value.replace(/,/g, '');
            }
            else{
                value = refs[ref].refs.item.value;
            }
        }

        return value;
    }

    save(url, parent, type='new', nextUrl='', callback=''){
        let data = {}, targetBTN = window.event.target;
        let {refs} = parent;

        window.$('#contentFrame').css('filter', 'blur(1px)');
        window.$(targetBTN).addClass('btn-loading');
        window.$(targetBTN).attr('disabled', 'disabled');

        if(type=="edit" || type=="update"){
            data['_method'] = 'PUT';
        }

        Object.keys(refs).forEach((ref)=>{
                if(refs[ref] && (refs[ref].refs || refs[ref].value)){
                    data[ref] = this.getRefValue(ref, parent);
                }
            }
        );

        axios.post(`${Config.getHost()}${url}`, data)
        .then((response)=>{
            let result = parseInt(response.data);
            let message = response.data;

            if(result != NaN){
                parent.setState({lastId: response.data});
                message =Lang('public.save_message');
            }
            NotificationManager.success(message, 'پیام', 5000);

            if(nextUrl != '')
            {
                let pathNextUrl = this.getSystemPrefix()+nextUrl;
                parent.props.history.replace(pathNextUrl);
            }
            if(typeof callback == "function"){
                callback(data, response.data);
            }
        })
        .catch((error)=>{
            if (error.response) {
                let title = "", message = "";
                // title = Lang('public.error message')
                switch(error.response.status){
                    case 422:
                        parent.setState({errors: error.response.data.errors});
                        message = Lang('public.error-422');
                        break;
                    case 401:
                        parent.setState({errors: error.response.data.errors});
                        message = Lang('public.error-401');
                        break;
                    case 501:
                        message = Lang('public.error-501');
                        break;
                }
                NotificationManager.error(message, title, 5000);

            } else if (error.request) {
                // console.log(error.request);
            } else {
                NotificationManager.error(Lang('public.Connection error'), Lang('public.error message'), 5000);
            }
        })
        .then(()=>{
            setTimeout(()=>{
                window.$(targetBTN).removeClass('btn-loading');
                window.$(targetBTN).attr('disabled', false);
                window.$('#contentFrame').css('filter', 'blur(0)');
            }, 500);
        });
    }

    destroy(e){
        let result = window.confirm(Lang('public.Are you sure you want to delete?'));
        let url = `${Config.getHost()}`+e.currentTarget.attributes['data-url'].value;
        if(result == true){
            const options = {
                url,
                method: "POST",
                header: {
                    'content-type': 'multipart/form-data'
                },
                data: {'_method':"delete"}
            }

            axios(options)
                .then((response)=>{
                    let rand = Math.random()*10000;
                    NotificationManager.success(response.data, 'پیغام', 5000);
                    this.props.history.push(this.props.history.location.pathname+`?${rand}`);
                })
                .catch((error)=>{
                    if(error.response.status == 404){
                        // alert('رکورد مورد نظر یافت نشد، احتمالا قبلا حذف گردیده است!!');
                        NotificationManager.error(Lang('public.The record was not found, probably deleted already'), 5000);
                        this.render();
                    }
                })
        }
        else{
            this.props.history.push(this.props.history.location.pathname+`?${rand}`);
        }
    }

    resetForm(parent){
        let {refs} = parent;
        parent.refs.forEach();
    }

    getSystemPrefix(){
        if(!Config.hasPrefix()) return "";

        let systemPrefix = "";
        let url = window.location.href;
        let urlSegments= url.split('#');

        if(urlSegments.length == 2){
            let path = urlSegments[1].split("/");
            systemPrefix = "/"+path[1];
        }
        return systemPrefix;
    }

    getInfo(url, parent, variableName = "items", callback=null){
        window.$("#loader").removeClass("d-none");
        const options = {
            url:Config.getHost()+url,
            method: "GET",
            header: {
                'content-type': 'application/json'
            }
        }

        axios(options)
            .then((response)=>{
                parent.setState({[variableName]: response.data});
                if(callback != null){
                  callback(response.data);
                }
            })
            .catch((error)=>{
                if (error.response) {
                    let title = "", message = "";
                    // title = Lang('public.error message');
                    switch(error.response.status){
                        case 422:
                            message = Lang('public.error-422');
                            break;
                        case 401:
                            message = Lang('public.error-401');
                            break;
                    }
                    NotificationManager.error(message, title, 5000);
                }
            })
            .then(()=>{
                setTimeout(()=>{
                    window.$("#loader").addClass("d-none"),
                    parent.setState({loading: ""})
                }, 400);
            });
    }

    archive(e)
    {
        let result = window.confirm(Lang('Move the ad to the archive list?'));
        let url = `${Config.getHost()}`+e.currentTarget.attributes['data-url'].value;
        if(result == true){
            const options = {
                url,
                method: "POST",
                header: {
                    'content-type': 'multipart/form-data'
                }
            }
            axios(options)
            .then((response)=>{
                let rand = Math.random()*10000;
                NotificationManager.success(response.data, 'پیغام', 5000);
                this.props.history.push(this.props.history.location.pathname+`?${rand}`);
            })
            .catch((error)=>{
                if(error.response.status == 422){
                    alert(Lang('You can only archive verified ads!'));
                    this.render();
                }
            })
        }
    }
}

let DataObj = new Data();
export {Data, DataObj};
