import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import { Pic, Lang, UserInfo ,Tools} from '../index';

class Header extends Component{
    constructor(props){
        super(props);
    }

    render(){
        let user = UserInfo();

        let name = user.name + " " +user.family;
        let panel = 'panel';
        if(user['panel']=='manage') panel = 'manage';

        let avatar = "/media/persons/"+user.pic;

        return(
            <nav className="navbar blueNav fixed-top">
                <Link to={"/"+panel} className="navbar-logo">
                    <span className="d-xs-block">
                        <img src="/themes/saba/img/logo.png" className="headerLogo" />
                    </span>
                </Link>
                <div className="ml-auto">
                    <div className="user d-inline-block">
                        <button className="btn btn-empty p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span>
                                <Pic src={avatar} defaultimg='/themes/saba/img/user.png' />
                            </span>
                            <span className="name">{name}</span>
                        </button>
                        <div className="dropdown-menu dropdown-menu-right">
                            <Link className="dropdown-item" to={"/"+panel}>{Lang('public.my_profile')}</Link>

                            {Tools.getArray(user.ngos).map((item, index)=>{
                                    let ngoHash = Tools.base64Encode(`${item.id}-${item.pivot.role_id}`);
                                    return <Link className="dropdown-item" to={"/ngo?"+ngoHash}> {Lang('public.ngo')} {item.name}</Link>
                                })
                            }
                            <Link className="dropdown-item" to={"/"+panel+'/edit-profile'}>{Lang('public.edit_my_profile')}</Link>
                            <Link className="dropdown-item" to={"/"+panel+'/change-password'}>{Lang('public.change_password')}</Link>
                            <a className="dropdown-item" target="_blanck" href={"/fa"}>{Lang('public.main_page')}</a>
                            <Link className="dropdown-item" to="/exit">{Lang('public.exit')}</Link>
                        </div>
                    </div>
                    {/* <a className="dropdown-item" target="_blanck" href={"/fa"}>{Lang('public.main_page')}</a> */}

                    <div className="header-icons d-inline-block align-middle">
                        <a className="btn btn-primary d-sm-inline-block" target="_blanck" href={"/fa"}>{Lang('public.main_page')}</a>
                    </div>
                </div>
                <a href="#" className="menu-button d-none d-md-block">
                    <i className="glyph-icon flaticon-menu"></i>
                </a>
                <a href="#" className="menu-button-mobile d-xs-block d-sm-block d-md-none">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 17">
                        <rect x="0.5" y="0.5" width="25" height="1"/>
                        <rect x="0.5" y="7.5" width="25" height="1"/>
                        <rect x="0.5" y="15.5" width="25" height="1"/>
                    </svg>
                </a>
            </nav>
        );
    }
}

export {Header};
