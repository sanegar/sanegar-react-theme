import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import {BreadCrumb} from './BreadCrumb';
import {LogOut} from '../Base/LogOut';

class Routes extends Component{
    render(){
      let {routes, Components} = this.props;
      let groupName = this.props.group;

      return(
          <main>
              <div className="container-fluid">
                  <div className="row">
                      <div className="col-12">
                          <Route path="/:path" component={(props)=><BreadCrumb {...props} routes={routes} />} />
                          <Route path="/exit" component={LogOut} />
                          {/* <div className="separator mb-5"></div> */}
                          {
                              routes.map((route, index)=> {
                                  if(route.namespace != undefined && route.component != undefined){
                                    if(Components[route.namespace])
                                      if(typeof Components[route.namespace][route.component] == "function"){
                                          let Module = Components[route.namespace][route.component];
                                          let path = "";
                                          if(groupName == undefined){
                                              let group = route.group;
                                              if(group == "@")
                                                  group = "";
                                              else if(group != null && group.substr(0,1) != "/")
                                                  group = "/"+group;
                                              path = group+route.path;
                                          }
                                          else{
                                              path = "/"+groupName+route.path
                                          }

                                          let pathItems = path.split('/');
                                          if(pathItems[pathItems.length-1].substr(0, 1) == ":")
                                              path = path+"(\\\d+)";

                                          return <Route
                                              path={path}
                                              key={index}
                                              component={(props)=><Module {...props} />}
                                              // exact={route.exact=="true"?true:false} />
                                              exact={route.exact=="true"?true:false} />
                                      }
                                  }
                              })
                          }
                      </div>
                  </div>
              </div>
          </main>
      );
    }
}

export default Routes;
