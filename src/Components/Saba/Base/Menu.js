import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import{Lang} from './../';

class Menu extends Component{
    constructor(props){
        super(props);
        this.state = {lastMenus: [], menuCategory: ""};
        this.checkActiveChild = this.checkActiveChild.bind(this);
        this.checkActiveParent = this.checkActiveParent.bind(this);
    }

    checkActiveParent(childs=[], category){
        let className = "";
        childs.forEach(item => {
            if(item.method!= undefined)
                if("/"+category+item.method.react_path == this.props.path){
                    className = "active";
                }
        });

        return className;
    }

    componentDidMount(){
        window.runMenu();
    }

    checkActiveChild(path){
        if(this.props.path == path)
            return "active"
        else
            return ""
    }

    shouldComponentUpdate(nextProps, nextState){
        if(this.state.lastMenus.length == 0) return true;

        let items = this.props.path.split('/');
        let category = items[1];
        let menuItems = [];
        nextProps.menus.forEach((catItem)=>{
            if(catItem.name == category)
                menuItems = catItem.menus;
        });

        if(menuItems.length > this.state.lastMenus.length || category != this.state.menuCategory) return true;
        else return false;
    }

    render(){

      let {menus, path, translate, groupCheck} = this.props;

      let items = path.split('/');
      let category = items[1];

      var menuItems = [];
      if(translate == undefined)
        translate = true;

      if(groupCheck == undefined || groupCheck == true){
          menus.forEach((catItem)=>{
              if(catItem.name == category)
                  menuItems = catItem.menus;
          });
      }else{
          menus.forEach((catItem)=>{
              menuItems = catItem.menus;
          });
      }

      this.state.lastMenus = menuItems;
      this.state.menuCategory = category;

      return(
        <div className="sidebar">
            <div className="main-menu">
                <div className="scroll">
                    <ul className="list-unstyled">
                        {
                          menuItems.map((rmenu, index)=>
                              <li data-link={rmenu.name} key={index} className={this.checkActiveParent(rmenu.childs, category)}>
                                  <a>
                                      <i className={rmenu.icon_class}></i>
                                      <span> {translate?Lang('public.'+rmenu.name):rmenu.label} </span>
                                  </a>
                              </li>
                            )
                        }
                    </ul>
                </div>
            </div>

            <div className="sub-menu">
                <div className="scroll ps">
                    {menuItems.map((item, index)=>
                        <ul className="list-unstyled" key={index} data-link={item.name}>
                            {(item.childs!=undefined?item.childs:[]).map((menu, i)=>{
                                if(menu.method != undefined)
                                {
                                    let path = "/"+category+menu.method.react_path;
                                    if(menu.method.react_path.substr(0, 1) == "@")
                                        path = menu.method.react_path.replace("@", "");

                                    return <li key={i} className={this.checkActiveChild(path)}>
                                        <Link to={path}>
                                            <i className={"fas fa-fw "+ menu.icon_class}></i>
                                            <span>{translate?Lang('public.'+menu.name):menu.label}</span>
                                        </Link>
                                    </li>
                                }
                            })}
                        </ul>
                    )}
                </div>
            </div>
        </div>
        );
    }
}

export default Menu;
