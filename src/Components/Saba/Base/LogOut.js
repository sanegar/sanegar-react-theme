import React, {Component} from 'react';
import {Config} from '../index';

class LogOut extends Component{
    constructor(props){
        super(props);
    }

    componentDidMount(){
        window.document.getElementById('logout-form').submit();
    }

    render(){
        let token = window.$('meta[name="csrf-token"]').attr('content');
        return(
          <React.Fragment>
              <div style={{textAlign:"center"}}>
                  خروج از سیستم مدیریت
              </div>
              <form id="logout-form" action={Config.setLogoutPath()} method="POST" style={{display: "none"}}>
                  <input type="hidden" value={token} name="_token" />
              </form>
          </React.Fragment>
        );
    }
}

export {LogOut};
