import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {singular, Cookies, Lang, Config, DIR} from '../index';
import axios from 'axios';

class BreadCrumb extends Component{
  constructor(props){
    super(props);
    let dir = DIR();

    this.state = {
        location: '',
        btnDisplay:"none",
        pathAssets: [],
        current: {},
        params:null,
        dir: dir
    };
    this.handlePAth = this.handlePAth.bind(this);
  }

  componentWillMount() {
    this.handlePAth(this.props.location.pathname);
  }

  componentDidUpdate(prevProps, prevState) {
    this.handlePAth(this.props.location.pathname);
  }

  handlePAth(path){
      // For Aviod LOOP
    if(this.state.location == path)
      return;

    let {routes} = this.props;

    window.$('#newDIV').css('display', 'none');

    let urlSegments = path.split("/");
    let suburls     = [{route:"/", url:"/"}];
    let breadcrumb  = [];

    let lastUrl = "", lastRoute = "";
    let start = 1;
    // if(urlSegments[1] == "panel" || urlSegments[1] == "ngo") start = 2;
    for(let i = start; i < urlSegments.length; i++){
        let name = null;
        if(parseInt(urlSegments[i]) > 0){
            let varName = singular(urlSegments[i-1]);
            lastRoute += "/"+`:${varName}Id`;
            name = `${varName}Title`;
        }else{
            if(urlSegments[i] == "panel") urlSegments[i] = "profile";
            lastRoute += "/"+urlSegments[i];
        }
        lastUrl += "/"+urlSegments[i];
        suburls.push({route:lastRoute, url:lastUrl, name});
    }

    let current = {};
    routes.forEach((route)=>{
      suburls.forEach((suburl, index)=>{
          if(suburl.url == path){
              current = {path: suburl.url, name: suburl.name, label: route.label, subLabel: route.subLable};
          }

          if("/" + route.group + route.path == suburl.route){
              breadcrumb.push({path: suburl.url, name: suburl.name, label: route.label, subLabel: route.subLable});
              delete suburls[index];
          }
      })
    });

    this.setState({pathAssets: breadcrumb, location: path, current});
  }

  static setLabel(id, value){
      window.$(`#${id}`).html(value);
      window.$(`#${id}Container`).css("display", "block");
      Cookies.set(id, value);
  }

  static getParams(path){
      // let path = this.props.location.pathname;
      let urlSegments = path.split("/");
      let variables = {};
      let params = [];

      for(let i=1; i < urlSegments.length; i++){
        if(parseInt(urlSegments[i]) > 0){
            let varName = singular(urlSegments[i-1]);
            variables[varName] = urlSegments[i];
            params.push(`${varName}=${urlSegments[i]}`);
        }
      }

      // return {variables, params: params.join('&')};
      return params.join('&');
  }

  static processLabels(path){
      let params = BreadCrumb.getParams(path);
      axios.get(`${Config.getHost()}/breadcrumb?${params}`)
        .then((response)=>{
            Object.keys(response.data).forEach((item)=>{
                BreadCrumb.setLabel(item, response.data[item]);
            });
        })
        .catch((error)=>{
            console.log(error);
        });
  }

  getLabel(id){
      Cookies.get(id);
  }

  render(){
      const {pathAssets, btnDisplay, current, dir} = this.state;
      let count = pathAssets.length;
      if(count == 0) return(<div></div>);

      return(
          <React.Fragment>
              {/* <h1 className="formTitle">{pathAssets[count-1].label} {Lang('public.'+pathAssets[count-1].label)}</h1> */}
              {/* <h1 className="formTitle">{Lang('public.'+pathAssets[count-1].subLabel)} {Lang('public.'+pathAssets[count-1].label)}</h1> */}
              <h1 className="formTitle">{Lang('public.'+current.subLabel)}</h1>
              <nav className="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                  <ol className="breadcrumb pt-0">
                      {
                          pathAssets.map((path, index)=>{
                              if(path.name == null)
                                  return <li key={"bread"+index} className={(index+1 == count)? "breadcrumb-item active":"breadcrumb-item"}>
                                      <Link key={"link"+index} to={path.path.replace("profile", "panel")}> {Lang('public.'+path.label)} </Link>
                                  </li>
                              else{
                                  return <li key={"bread"+index}
                                             style={{display: this.getLabel(path.name)?"block":"block"}}
                                             id={path.name+'Container'}
                                             className={(index+1 == count)? "breadcrumb-item breadcrumb-detail active":"breadcrumb-item breadcrumb-detail"}>
                                              <Link key={"link"+index} id={path.name} to={path.path.replace("profile", "panel")}> {this.getLabel(path.name)} </Link>
                                          </li>
                              }
                          })
                      }
                  </ol>
              </nav>
              <div className={(dir == "rtl")? "float-sm-right text-zero" : "float-sm-left text-zero"} id="newDIV" style={{display:btnDisplay}}>
                  <a style={{color:"#FFF"}} href="" id="newBTN" className="btn btn-primary btn-lg ml-1"> مورد جدید </a>
              </div>
          </React.Fragment>
      );
  }
}

export {BreadCrumb};
