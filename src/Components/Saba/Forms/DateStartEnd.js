import React from 'react';
import {Element} from './Element';

export class DateStartEnd extends Element {
    constructor(props){
        super(props);
    }

    componentDidMount(){
        window.$('.datepicker').persianDatepicker();
        window.$('#datepicker').persianDatepicker({
            inline: false,
        });

        window.$('.date').persianDatepicker();

        window.$('.date-inline').persianDatepicker({
            inline: true,
        });

        window.$('.calendar').persianDatepicker({
            inline: true,
        });
    }

    render(){
        let {label, name, defaultValue, error, placeholder, onChange,className,type } = this.props;
        let divError = this.checkError();

        return(
            <React.Fragment>
                <div className="form-group mb-3">
                    <label>محدوده تاریخ</label>
                    <div className="input-daterange input-group" id="datepicker">

                        <input type="text" className="input-sm form-control" name="start" placeholder="شروع"/>
                        <span className="input-group-addon"></span>
                        <input type="text" className="input-sm form-control" name="end" placeholder="پایان"/>
                    </div>
                    {divError}
                </div>
            </React.Fragment>
        );
    }
}
