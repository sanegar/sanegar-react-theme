import React from 'react';
import axios from 'axios';
import {Element, Config} from '../index';
import DropzoneComponent from 'react-dropzone-component';

class Dropzones extends Element{

  constructor(props) {
      super(props);
      this.state = {
        files: [],
      };
      this.getFromProps        = this.getFromProps.bind(this);
      this.getEventHandler     = this.getEventHandler.bind(this);
      this.processDefaultValue = this.processDefaultValue.bind(this);
  }

  getFromProps(){
      let {label, name, className, icon, url, deleteUrl, defaultValue, uploadDir, maxFiles, maxFilesize, error,
          required, acceptType, maxImageWidth, maxImageHeight, minImageWidth, minImageHeight} = this.props;

      if(acceptType == undefined || acceptType == null || acceptType =="")
          acceptType = "image/*,application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document,video/*";
      if(maxFiles == undefined || maxFiles == null || maxFiles == "")
          maxFiles = 1;
      if(maxFilesize == undefined || maxFilesize == null || maxFilesize == "")
          maxFilesize = 1000;

      return {label, name, className, icon, url, deleteUrl, defaultValue, uploadDir, maxFiles, maxFilesize, error,
          required, acceptType, maxImageWidth, maxImageHeight, minImageWidth, minImageHeight};
  }

  getFileName(fileName){
      let files;

      if(typeof fileName == "string"){
          fileName = fileName.replace(/###/g, "");
          files = fileName.split('/');
      }
      else{
          files = fileName.name.split('/');
      }

      return files[files.length - 1];
  }

  processDefaultValue(){
      let {defaultValue, uploadDir} = this.props;

      /**
       * Used for hidden input
       */
      let vals = "";
      if(defaultValue != undefined && defaultValue != ""){
          // For Array type
          if(typeof defaultValue != "string"){
              // Like [file1, file2, ...]
              if(defaultValue.url != undefined){
                  vals = defaultValue.join("###");
                  if(vals != "") vals = vals + "###";
              }
              // Like [{url, extension}, {url, extension}, ...]
              else{
                  defaultValue.forEach((item)=>{
                      if(item.url != undefined && item.name == undefined)
                          vals += item.url+"."+item.extension+"###";
                      else
                          vals += this.getFileName(item)+"###";
                  });
              }
          }
          // For String type Like file.png
          else{
              vals = defaultValue;
          }
      }

      /**
       * Used for set default values in dropzone object
       */
      let initDropzone = (myDropzone)=>{
          if(defaultValue != undefined && defaultValue != ""){
              if(typeof defaultValue == "string"){
                  let files = defaultValue.split('###');
                  files.forEach(element => {
                      if(element != ""){
                          let name = this.getFileName(element);
                          let path = uploadDir?uploadDir:"";
                          path = path+name;

                          var mockFile = {name: name, size: element.size};
                          myDropzone.options.addedfile.call(myDropzone, mockFile);
                          myDropzone.options.thumbnail.call(myDropzone, mockFile, path);
                      }
                  });
              }
              else{
                  defaultValue.forEach(element => {
                      let name = "";
                      if(element.url != undefined && element.name == undefined)
                          name = element.url+"."+element.extension;
                      else
                          name = this.getFileName(element);

                      let path = uploadDir?uploadDir:"";
                      path = path+name;
                      var mockFile = {name: name, size: element.size};
                      myDropzone.options.addedfile.call(myDropzone, mockFile);
                      myDropzone.options.thumbnail.call(myDropzone, mockFile, path);
                  });
              }
          }
      }

      return {vals, initDropzone};
  }

  getLangVars(){
      let langs = {
          dictDefaultMessage: "رها کردن فایل جهت آپلود",
          dictFallbackMessage: "مرورگر شما، عمل کشیدن و رها کردن عکس را پشتیبانی نمی کند!!",
          dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
          dictFileTooBig: "حجم فایل از حدمجاز بیشتر می باشد ({{filesize}}MiB). حداکثر حجم فایل: {{maxFilesize}}MiB.",
          dictInvalidFileType: "آپلود فایل از این نوع مجاز نمی باشد.",
          dictResponseError: "سرور پاسخی با کد {{statusCode}} ارسال کرده است.",
          dictCancelUpload: "لغو آپلود",
          dictCancelUploadConfirmation: "آیا از کنسل شدن آپلود مطمئن هستید?",
          dictRemoveFile: "حذف فایل",
          dictMaxFilesExceeded: "شما نمی توانید فایل های بیشتری آپلود نمایید.",
      };
      return langs;
  }

  getDropzoneConfig(){
      let {maxFiles, maxFilesize, acceptType, maxImageWidth, maxImageHeight, minImageWidth, minImageHeight} = this.getFromProps();
      let langs = this.getLangVars();

      var djsConfig = {
          autoProcessQueue: true,
          maxFiles: maxFiles,
          maxFilesize: maxFilesize,
          addRemoveLinks: true,
          acceptedFiles: acceptType,
          ...langs,
          init: function () {
              this.on("success", function(file, responseText) {
                  file.previewTemplate.setAttribute('id',responseText[0].id);
              });
              this.on("thumbnail", function(file) {
                  if (maxImageWidth != undefined && maxImageWidth != '' && (file.width > maxImageWidth || file.height > maxImageHeight)) {
                      file.rejectMaxDimensions()
                  }
                  else if (minImageWidth != undefined && minImageWidth != "" && (file.width < minImageWidth || file.height < minImageHeight)) {
                      file.rejectMinDimensions()
                  }
                  else {
                      file.acceptDimensions();
                  }
              });
          },
          // accept: function(file, done) {
          //     file.acceptDimensions = done;
          //     file.rejectMinDimensions = function() { done("طول و عرض تصویر حداقل باید  "+ minImageWidth+"×"+minImageHeight+" باشد!!"); };
          //     file.rejectMaxDimensions = function() { done("طول و عرض تصویر حداکثر باید "+ maxImageWidth+"×"+maxImageHeight+" باشد!!"); };
          // }
      }

      return djsConfig;
  }

  getEventHandler(initDropzone){
      let {deleteUrl} = this.props;
      var eventHandlers = {
          success:(file)=>{
              this.refs.item.value += file.xhr.response+"###";
              // console.log(file.xhr.response);
          },
          init: initDropzone,
          removedfile:(file)=>{
              // alert(file);
              let path = "";
              // console.log(file);
              if(file.xhr != null){
                  path = file.xhr.response;
              }
              else{
                  path = file.name;
              }
              axios.get(Config.getHost()+deleteUrl+path);
              this.refs.item.value = this.refs.item.value.replace(path+"###", "", "g");
          }
      }

      return eventHandlers;
  }

  render() {
      let {className, url} = this.props;
      let {vals, initDropzone} = this.processDefaultValue();

      let rand = Math.random() * 1000;
      let djsConfig   = this.getDropzoneConfig();
      let componentConfig  = {postUrl: Config.getHost()+url};
      let eventHandlers  = this.getEventHandler(initDropzone);
      let id = this.getId(),
            label = this.getLabel(),
            helpDiv = this.getHelp(),
            divError = this.checkError(),
            requiredDiv = this.getRequired();

      return (
        <div className={"form-group  dropzone-wrapper " + className}>
          <label className={"form-group has-float-label "}>
              <div className="form-group">
                  <DropzoneComponent
                        key = {rand}
                        djsConfig={djsConfig}
                        config={componentConfig}
                        eventHandlers={eventHandlers}
                  />
                  <input type="hidden" id={id} ref='item' defaultValue = {vals} />
              </div>
              <span>
                  {label}
                  {helpDiv}
                  {requiredDiv}
              </span>
          </label>
          {divError}
        </div>

      );
  }

}

export {Dropzones};
