import React from 'react';
import {Element} from './Element';
import Lang from '../Tools/Lang';

class Textarea extends Element {
  constructor(props){
      super(props);
      this.state = {value:""};
  }

  componentDidMount()
  {
    let {defaultValue} = this.props;
    let {id} = this.state;
    window.tinymce.init({
      selector: '#'+id,
      statusbar: false,
      menubar: false,
      height : 300,
      // plugins: 'code,image,table,imagetools,advcode,media,powerpaste,codesample,paste,textpattern',
      plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker textpattern noneditable help formatpainter pageembed charmap mentions quickbars linkchecker emoticons advtable imagetools',
      toolbar: 'code undo redo | bold italic alignleft aligncenter alignright | bullist numlist outdent indent image | ltr rtl',
      images_upload_url: 'upload.php',
      images_upload_base_path: 'http://localhost/projects/editor/tinymce_5.2.1/',
      images_upload_credentials: true
    });

    tinymce.activeEditor.uploadImages(function(success) {
      document.forms[0].submit();
    });
  }

  componentWillReceiveProps(props){
      let val = props.defaultValue;
      if(val != undefined && val != "" && val != null)
      {
        // this.refs.item.editor.setData(val);
      }
  }

  render(){
      let {label, defaultValue, className, row, options, required, help, placeholder, disabled} = this.props;
      let divError = this.checkError();
      let requiredDiv = '';
      let helpDiv = '';

      if(required == "true")
      {
          requiredDiv = <sup className="required text-danger"> * </sup>;
      }
      if(help != undefined)
      {
          helpDiv = <a href='#' alt={help}><i class="fa fa-question-circle" aria-hidden="true"></i></a>;
      }
      if (row==undefined) row = 5;
      let labelTranslated = Lang('public.'+label);
      return(
          <div className={"form-group curve "+className}>
              <label className="form-group has-float-label">
                  <textarea disabled={(disabled == undefined)?false:true} className="form-control" key={Math.random()*1000}
                      style={{paddingTop: '10px'}}
                      rows={row}
                      placeholder={Lang('public.'+placeholder, {title: labelTranslated})}
                      defaultValue = {defaultValue} ref='item'
                      onFocus = {()=>this.removeError()}
                      {...options}></textarea>
                  <span>
                      {requiredDiv}
                      {labelTranslated}
                      {helpDiv}
                  </span>
              </label>
              {divError}
          </div>
      );
  }
}

export {Textarea};
