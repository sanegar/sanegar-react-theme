import React, { Component } from 'react';
import isEqual from "react-fast-compare";
import {Tools, Lang} from '../index';

class Element extends Component{
    constructor(props){
        super(props);
        let rand = this.createRand();
        this.state = {
                      rand: rand,
                      errorRand: rand,
                      previousRef:null,
                      id: 'element-'+rand,
                      ignoreError: false,
                    };
        this.getCurrentRef = this.getCurrentRef.bind(this);
        this.getPreviousRef = this.getPreviousRef.bind(this);
        this.getDefaultValue = this.getDefaultValue.bind(this);
        this.getParent = this.getParent.bind(this);
        this.getRefName = this.getRefName.bind(this);
        this.checkError = this.checkError.bind(this);
        this.removeError = this.removeError.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState){
        let changed = false,
            changedProps = [],
            // propsNeedsTocheck = ['defaultValue', 'label', 'children', 'error', 'className', 'data', 'multiple', 'disabled'],
            propsNeedsTocheck = ['defaultValue', 'label', 'data', 'children', 'className', 'multiple', 'disabled'],
            proceed = (element)=>{
                changed = true;
                changedProps.push(element);
                this.getCurrentRef();
            };
        if(nextProps.error != undefined){
            proceed();
        }

        if(nextState.currentType){
            if(nextState.currentType != this.state.currentType){
                this.getPreviousRef();
                return true;
            }
        }

        propsNeedsTocheck.forEach((key)=>{
            if(typeof nextProps[key] != typeof this.props[key]) {
                proceed(key);
            }
            else if(Tools.isArray(nextProps[key])){
                if(nextProps[key].length != this.props[key].length){
                    proceed(key);
                }
                else if(key == "children" && nextProps.children != undefined){
                    if(!isEqual(nextProps.children, this.props.children)){
                        proceed(key);
                    }
                }
                else if(this.props[key] != undefined){
                    nextProps[key].forEach((item, index)=>{
                        if(!isEqual(item, this.props[key][index])){
                            proceed(key);
                        }
                    });
                }
            }
            else if(nextProps[key] instanceof Object){
                let str1 = JSON.stringify(nextProps[key]);
                let str2 = JSON.stringify(this.props[key]);

                if(str1 != str2){
                    proceed(key);
                }
            }
            else{
                if(nextProps[key] != this.props[key]){
                    proceed(key);
                }
            }
        });

        return(changed);
    }

    checkError(){
        let {error} = this.props,
            {errorRand} = this.state,
            divError = [];

        if(error!=undefined){
            this.getPreviousRef();
            let err = error.join('<br/>');
            this.state.ignoreError = false;
            let element = <div className='invalid-tooltip' id={'error-'+errorRand} key={errorRand} style={{display:"block"}}>
                            <span dangerouslySetInnerHTML={{__html: err}} />
                          </div>
            divError.push(element);
        }
        return divError;
    }

    getParent(){
        var parent = this._reactInternalFiber._debugOwner.stateNode;
        return parent;
    }

    getRefName(){
        var name = this._reactInternalFiber.ref._stringRef;
        return name;
    }

    removeError(){
        let {rand} = this.state;
        let parent = this.getParent();
        let {errors} = parent.state;
        let refName = this.getRefName();
        delete errors[refName];
        parent.setState({errors});

        let {errorRand} = this.state;
        if((window.$('#error-'+errorRand)).length > 0){
            window.$('#error-'+errorRand).slideUp(400, ()=>{
                this.state.errorRand = this.createRand();
            });
        }
    }

    getCurrentRef(){
        return this.state.previousRef = this.refs.item;
    }

    getPreviousRef(){
        return (this.state.previousRef != null) ? this.state.previousRef.value : "";
    }

    getDefaultValue(){
        let {defaultValue} = this.props, value = defaultValue;
        if(this.getPreviousRef()){
            value = this.getPreviousRef();
        }
        return value;
    }

    getHelp(){
      let {help} = this.props;
      if(help)
        return <a href='#' alt={help}><i class="fa fa-question-circle" aria-hidden="true"></i></a>;

      return '';
    }

    getRequired(){
      let {required} = this.props;
      if(required == "true" || required === true)
        return <sup className="required text-danger"> * </sup>;

      return '';
    }

    getId(){
        return this.props.id || this.state.id;
    }

    getLabel(){
        let {label} = this.props;
        return Lang('public.'+label);
    }

    getPlaceHolder(){
        let {placeholder} = this.props;
        return (placeholder)?Lang('public.'+placeholder, {title: this.getLabel()}) : "";
    }

    createRand(){
        return (new Date()).getTime() + Math.ceil(Math.random()*10000);
    }

    render(){
        return <div>Parent Element!!</div>
    }
}

export {Element};
