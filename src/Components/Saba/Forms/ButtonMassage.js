import React from 'react';

export const ButtonMassage = (props) => {
    let {label, className, type, onClick} = props;

    return(
        <button className={className} type={type} onClick={(event)=>{
                        window.event = event;
                        onClick(event)}
                    }>
            <i className="icon-paper-plane font-lg"></i>
            {label}
            <div className="paper-ripple"><div className="paper-ripple__background"></div><div className="paper-ripple__waves"></div></div>
            <span className="loading"></span>
        </button>
    );
}
