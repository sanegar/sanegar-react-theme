import React from 'react';
import {Element, Lang} from '../index';

export class RadioBtn extends Element {
    constructor(props){
        super(props);
        // this.state = {checkDefaultValue: true};
        this.setDefaultValue = this.setDefaultValue.bind(this);
    }

    setDefaultValue(){
        let {name} = this.props;
        let {refs} = this;
        let radios = document.getElementsByName(name);
        let selected = false;
        let defaultValue = this.getDefaultValue()
        // console.log(this.refs);
        Object.keys(refs).forEach((elem)=>{
          if(refs[elem].checked) selected = true;
        });
        if(!selected)
          radios.forEach((item)=>{
              if (item.value == defaultValue){
                  document.getElementById(item.id).checked = 'checked';
              }
          });
    }

    componentDidMount(){
        this.setDefaultValue();
    }

    componentWillReceiveProps(props){
        this.setDefaultValue();
    }

    render(){

        let {name, data, classNameDiv, classIcon, onChange} = this.props;
        let label = this.getLabel(),
            helpDiv = this.getHelp(),
            divError = this.checkError(),
            requiredDiv = this.getRequired();

        return(<div className={classNameDiv}>
            <h6>
                <i className={"glyph-icon "+ classIcon}></i>
                {label}
                {helpDiv}
                {divError}
                {requiredDiv}
            </h6>
            <div className="mb-4 pr-5">
                {
                    data.map((item, index)=>
                        <div className={"custom-control custom-radio "+item.className} key={index}>
                            {
                                <input
                                    key={index}
                                    name={name}
                                    type="radio"
                                    id={name+index}
                                    ref={'item'+index}
                                    value={item.value}
                                    onChange={onChange}
                                    className={"custom-control-input "}
                                />
                            }
                            <label className={"custom-control-label "} htmlFor={name+index}>
                                {Lang('public.'+item.label)}
                            </label>
                        </div>
                    )
                }

            </div>
        </div>

        );
    }
}
