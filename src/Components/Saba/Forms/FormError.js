import React, {Component} from 'react';

class FormError extends Component {
    // constructor(props) {
    //     super(props);
    // }

    render() {
        let {forHTML, error} = this.props;

        return (
            <div forHTML={forHTML} style={{color:'red', fontSize:'120%', fontWeight:'700', marginBottom:'0.85em'}}>
                {error}
            </div>
        );
    }
}

export {FormError};