import React from 'react';
import {Element} from '../index';

export class DatePicker extends Element {
    componentDidMount(){
        setTimeout(()=>{
               if (this.props.options!=undefined) {
                    let {onChange,data} = this.props.options;
                    data = data.replace(".","-");
                    window.$("."+data+' .datepicker').persianDatepicker({
                        format: 'YYYY/MM/DD',
                        inline: false,
                        initialValue: false,
                        onSelect:()=>{
                            let target = $("."+data+' .datepicker')[0];
                            let e = {target}
                            onChange(e);
                        }
                    })
                }else{
                    window.$('.datepicker').persianDatepicker({
                        format: 'YYYY/MM/DD',
                        inline: false,
                        initialValue: false,
                    })
                }
            }
        , 1000);
    }

    render(){
        let {name, className, options} = this.props;
        let id = this.getId(),
            data = "",
            label = this.getLabel(),
            helpDiv = this.getHelp(),
            divError = this.checkError(),
            requiredDiv = this.getRequired(),
            placeholder = this.getPlaceHolder(),
            defaultValue = this.getDefaultValue();

        if(options!=undefined){
          data = options.data;
          delete options.data;
        }
        return(
            <React.Fragment>
                <div className={className}>
                    <label className={"form-group has-float-label "}>
                        <input
                            id={id}
                            ref='item'
                            name={name}
                            placeholder = {placeholder}
                            defaultValue = {defaultValue}
                            className="form-control datepicker"
                            data={data}
                            {...options}
                        />
                        <span>
                            {label}
                            {helpDiv}
                            {requiredDiv}
                        </span>
                        {divError}
                    </label>
                </div>
            </React.Fragment>
        );
    }
}
