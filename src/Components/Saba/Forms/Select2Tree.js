// import React from 'react';
import {Select2} from './Select2';
import {Tools} from './../index';

class Select2Tree extends Select2{
    constructor(props){
        super(props);
        this.state.dataType = "select2tree";
        // this.createSelect2 = this.createSelect2.bind(this);
    }

    createSelect2(){
        let {maxSelected, data, options} = this.props;
        let id = this.getId(),
            self = this,
            placeholder = this.getPlaceHolder(),
            defaultValue = this.getDefaultValue();

        if(Tools.isArray(defaultValue)){
            let temp = [];
            defaultValue.map((item)=>{
                temp.push(item.id);
            });
            defaultValue = temp;
        }

        let config = {
            treeData: {dataArr: data}, 
            maximumSelectionLength: maxSelected?maxSelected:10,
            theme: "bootstrap",
            placeholder: placeholder
        };

        window.$("#"+id)
            .select2ToTree(config)
            .on("select2:select",(e)=>{
                if (options!=undefined) {
                    options.onChange(e)
                }
            }).on('select2:opening', function (evt) {
                self.removeError();
            });
        
        if(defaultValue != undefined)
            window.$(`#${id}`).val(defaultValue).trigger("change");
    }
}

export {Select2Tree};
