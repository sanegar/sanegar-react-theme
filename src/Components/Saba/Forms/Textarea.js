import React from 'react';
import {Element} from '../index';

class Textarea extends Element {

    render(){
        let {className, row, options, disabled} = this.props;
        let {rand} = this.state;
        let id = this.getId(),
            label = this.getLabel(),
            helpDiv = this.getHelp(),
            divError = this.checkError(),
            requiredDiv = this.getRequired(),
            placeholder = this.getPlaceHolder(),
            defaultValue = this.getDefaultValue();

        if (!row) row = 5;

        return(
            <div className={"form-group curve "+className}>
                <label className="form-group has-float-label">
                    <textarea
                        id = {id}
                        ref = 'item'
                        key={"textarea-"+rand}
                        placeholder = {placeholder}
                        defaultValue = {defaultValue}
                        rows = {row}
                        className = "form-control"
                        style = {{paddingTop: '10px'}}
                        disabled = {disabled?true:false}
                        onFocus = {()=>this.removeError()}
                        {...options}>
                    </textarea>
                    <span>
                        {label}
                        {requiredDiv}
                        {helpDiv}
                    </span>
                </label>
                {divError}
            </div>
        );
    }
}

export {Textarea};
