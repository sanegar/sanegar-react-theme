import React, {Component} from 'react';
import {Element} from '../index';

export class Clock extends Element {

    componentDidMount()
    {
      this.init();
    }

    componentDidUpdate(){
      this.init();
    }

    init(){
      window.$(".clockpicker").clockpicker({
          "donetext": "قبول"
      });
    }

    render() {

        let {name, className} = this.props;
        let rand = Math.round(Math.random()*100000);
        let id = this.getId(),
            label = this.getLabel(),
            helpDiv = this.getHelp(),
            divError = this.checkError(),
            requiredDiv = this.getRequired(),
            placeholder = this.getPlaceHolder(),
            defaultValue = this.getDefaultValue();

        return (
            <div className={"portlet-body "+className}>
                <div className="form-group">
                    <label>{label}</label>
                    <div className="input-group">
                        <span className="input-group-addon">
                            <i className="glyph-icon fas fa-clock"></i>
                        </span>
                        <input
                            id = {id}
                            ref='item'
                            name={name}
                            type="text"
                            key={'clock'+rand}
                            defaultValue = {defaultValue}
                            className="form-control clockpicker"
                            placeholder={placeholder}
                        />
                    </div>
                    {divError}
                    {requiredDiv}
                    {helpDiv}
                </div>
            </div>
        );
    }
}
