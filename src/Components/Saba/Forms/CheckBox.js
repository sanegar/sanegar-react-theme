import React from 'react';
import {Element} from './../index';


export class CheckBox extends Element {
    constructor(props){
        super(props);
    }

    render(){
        let {name, value, className ,onChange} = this.props;
        let rand = Math.random(1000, 9999);
        let id = this.getId(),
            label = this.getLabel(),
            helpDiv = this.getHelp(),
            divError = this.checkError(),
            requiredDiv = this.getRequired(),
            defaultValue = this.getDefaultValue();

        return(
            <div className={"form-group "+className} >
                <label className="col-2 col-form-label">{label}</label>
                <div className="col-10">
                    <div className="custom-switch custom-switch-primary-inverse custom-switch-small pl-1">
                        <input
                            id={id}
                            ref='item'
                            value={value?value:1}
                            key={"checkbox-"+rand}
                            name={name}
                            type="checkbox"
                            onChange={onChange}
                            className="custom-switch-input"
                            defaultChecked={defaultValue==1?true:false}
                        />
                        <label className="custom-switch-btn" htmlFor={id} dir="ltr">
                            {label}
                        </label>
                        {helpDiv}
                        {divError}
                        {requiredDiv}
                    </div>
                </div>
            </div>
        );
    }
}
