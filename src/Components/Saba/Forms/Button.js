import React from 'react';
import Lang from '../Tools/Lang';

export const Button = (props) => {
  let {label, onClick, className, disabled} = props;

  className == undefined ?className="btn btn-primary":className=className;
  if(disabled) className += " disabled";

  return(
      <button className={className} type="button" disabled={disabled?true:false} onClick={(event)=>{
                      window.event = event;
                      onClick(event)}
                  }>
          {Lang('public.'+label)}
          <span className="loading"></span>
      </button>
  );
}
