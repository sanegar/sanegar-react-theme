
import React, {Component} from 'react';

export class TabList extends Component{
  constructor(props){
      super(props);
      this.state = {errorNum: 0};

      this.getErrorsCount = this.getErrorsCount.bind(this);
      this.componentDidMount = this.componentDidMount.bind(this);
      this.componentWillReceiveProps = this.componentWillReceiveProps.bind(this);
      this.click = this.click.bind(this);
  }

  getErrorsCount(props){
    let {errors} = props;
    let num = 0;
    // console.log(errors);

    if(errors != undefined)
      // Tools.getValue(errors, "keys").forEach(element => {
      errors['keys'].forEach(element => {
          if(errors.errors[element] != "" && errors.errors[element] != undefined){
            num++;
          }
      });
    this.setState({errorNum: num});
  }

  componentWillReceiveProps(nextProps){
    this.getErrorsCount(nextProps);
  }

  componentDidMount(){
    this.getErrorsCount(this.props);
  }

  click(){
    this.setState({errorNum: 0});
  }

  render(){
      const {children, active, href} = this.props;
      const {errorNum} = this.state;

      return(
          <React.Fragment>
              <li className="nav-item text-center">
                  <a className={active?"nav-link active":"nav-link"} id="first-tab_" data-toggle="tab" href={"#"+href} role="tab" aria-controls="first" aria-selected="true" onClick={this.click}>
                      {
                          children
                      }
                      { (errorNum>0)?<div className="badge badge-danger" style={{margin:"auto 5px"}}>{errorNum}</div>:"" }
                  </a>
              </li>
          </React.Fragment>
      );
  }
}
