import React from 'react';
import {Element} from './../index';

export class Check extends Element {
    constructor(props){
        super(props);
    }

    render(){
        let {name, value, className, checked, onChange} = this.props;
        let rand = Math.ceil(Math.random()*10000);

        if(className == "") className='mb-3 col-md-2';

        let id = this.getId(),
            label = this.getLabel(),
            helpDiv = this.getHelp(),
            divError = this.checkError(),
            requiredDiv = this.getRequired(),
            defaultValue = this.getDefaultValue();

        return(
            <div className={className}>
                <div className="custom-control custom-checkbox mb-4">
                    <input
                        id={id}
                        ref='item'
                        name={name}
                        type="checkbox"
                        onChange={onChange}
                        key={'check' + rand}
                        value={value?value:1}
                        className="custom-control-input"
                        defaultChecked={(defaultValue==value||checked)?true:false}
                     />
                    <label className="custom-control-label" htmlFor={id}> {label} </label>
                </div>
                <span>
                    {requiredDiv}
                    {helpDiv}
                </span>
                {divError}
            </div>
        );
    }
}
