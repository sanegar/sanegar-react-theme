import React from 'react';
import {Element, Tools} from '../index';

export class Input extends Element {
    constructor(props){
        super(props);
        this.state.currentType = props.type;
        this.showHidePassword = this.showHidePassword.bind(this);
        this.state.iconStyle = {
            position: "absolute",
            top: "1rem",
            left: "1rem",
            cursor: "pointer"
        }
    }

    keyUpHandler(e){
      e.target.value = Tools.getAsCurrency(e.target.value);
    }

    showHidePassword(e){
        $(e.target).toggleClass("fa-eye fa-eye-slash");
        if(this.state.currentType === "password"){
            this.setState({currentType : "text"});
        }else{
            this.setState({currentType : "password"});
        }
    };

    render(){
        let {className, inputClassName, type, options, disabled, name, value, checked,
             max, min, maxlength} = this.props;
        let keyUp = ()=>{}, dataType = '';
        let {currentType, iconStyle} = this.state;
        let id = this.getId(),
            rand = Math.random(),
            label = this.getLabel(),
            helpDiv = this.getHelp(),
            divError = this.checkError(),
            requiredDiv = this.getRequired(),
            placeholder = this.getPlaceHolder(),
            defaultValue = this.getDefaultValue();

        if(type == 'currency' || type == "price"){
            type = "text";
            dataType = 'currency';
            keyUp = this.keyUpHandler;
            if(defaultValue)
              defaultValue = Tools.getAsCurrency(defaultValue);
        }
        else{
            if(options && options.onKeyUp){
              keyUp = options.OnKeyUp;
              delete options.OnKeyUp;
            }
        }

        if(type=="hidden"){
            return (
                <input
                    id={id}
                    ref='item'
                    type={type}
                    defaultValue={defaultValue}
                />
            )
        }

        return(
            <div className = {className}>
                <label className={"form-group has-float-label "}>
                    <input className={"form-control "+inputClassName}
                           id={id}
                           name={name}
                           ref='item'
                           key={'input-'+rand}
                           type={type == "password"?currentType:(type?type:"text")}
                           data-type = {dataType}
                           value={value}
                           defaultValue = {defaultValue}
                           placeholder={placeholder}
                           disabled={disabled==true?true:false}
                           max={max || maxlength}
                           min={min}
                           onKeyUp={keyUp}
                           onFocus={()=>this.removeError()}
                           checked={checked}
                           {...options}
                    />
                    <i className={ type =='password' ? 'glyph-icon far fa-eye': 'none'} style={iconStyle} onClick={this.showHidePassword}></i>
                    <span>
                        {requiredDiv}
                        {label}
                        {helpDiv}
                    </span>
                    {divError}
                </label>
            </div>
        );
    }
}
