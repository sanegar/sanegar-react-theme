class ConfigClass{
    constructor(){
      window.theme = 'Saba';
      window.themeHost = '';
      window.themeStore = {};
      window.themeBasePath = "/profile";
      window.logoutPath = "/admin/logout";
      window.hasPrefix = true;

      this.getHost = this.getHost.bind(this);
      this.getBasePath = this.getBasePath.bind(this);
      this.getStore = this.getStore.bind(this);
      this.getLogoutPath = this.getLogoutPath.bind(this);

      this.setHost     = this.setHost.bind(this);
      this.setBasePath = this.setBasePath.bind(this);
      this.setStore    = this.setStore.bind(this);
      this.setLogoutPath    = this.setLogoutPath.bind(this);
    }

    setTheme(theme){
      window.theme = theme;
    }

    setHost(host){
      window.themeHost = host;
    }

    setBasePath(path){
      window.themeBasePath = path;
    }

    setStore(store){
      window.themeStore = store;
    }

    setLogoutPath(path){
      window.logoutPath = path;
    }

    getTheme(){
        return window.theme;
    }

    getHost(){
        return window.themeHost;
    }

    getStore(){
      return window.themeStore;
    }

    getBasePath(){
      return window.themeBasePath;
    }

    getLogoutPath(){
      return window.logoutPath;
    }

    hasPrefix(value = null){
      if(value != null)
        window.hasPrefix = value;
      else
        return window.hasPrefix;
    }
}

const Config = new ConfigClass();

export default Config;
