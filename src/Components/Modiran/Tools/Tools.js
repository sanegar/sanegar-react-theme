class Tools{
    static getArray(obj){
        return (obj != undefined && obj != null)? obj : [];
    }

    static inArray(search, array){
        let find = false;
        array.forEach((item)=> {
            if(search == item) find = true
        });
        return find;
    }
}

export {Tools};
