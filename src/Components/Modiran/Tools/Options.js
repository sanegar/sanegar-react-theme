import Axios from "axios";
// import moment from "jalali-moment";
import Config from '../../Config';

class Options {
    static toPrice(str) {
        if (str != null & str != "" & str.toString().length > 3) {
            str = str.toString().replace(/\,/g, '');
            var objRegex = new RegExp('(-?[0-9]+)([0-9]{3})');

            while (objRegex.test(str)) {
                str = str.replace(objRegex, '$1,$2');
            }

        }
        if (str == null) {
            str = 0;
        }
        return str;
    }

    static toPriceInput(e) {
        let str = e.target.value;
        let valid = ["1","2","3","4","5","6","7","8","9","0"];
        let key = e.key;
        if(window.$.inArray(key,valid) != -1){
            if (str != null & str != "" & str.toString().length > 3) {
                str = str.toString().replace(/\,/g, '');
                var objRegex = new RegExp('(-?[0-9]+)([0-9]{3})');

                while (objRegex.test(str)) {
                    str = str.replace(objRegex, '$1,$2');
                }
            }
        }else{
            return false;
        }
        window.$(e.target).val(str);
    }
}

export { Options };
