import React, {Component} from 'react';
import {Link} from 'react-router-dom';
// import $ from 'jquery';
// import {routes} from '../../routes';

class BreadCrumb extends Component{
    constructor(props){
        super(props);
        this.state = {
            location: '',
            pathAssets: [],
        };
        this.handlePAth = this.handlePAth.bind(this);
    }

    componentWillMount() {
        // console.log(this.props.location);
        this.handlePAth(this.props.location.pathname);
    }

    componentDidUpdate(prevProps, prevState) {
        // console.log(prevProps.match.params.path);
        // console.log(this.props.location);
        this.handlePAth(this.props.location.pathname);
    }

    handlePAth(path){
        // For Aviod LOOP
        if(this.state.location == path)
        return;

        // $('#newDIV').css('display', 'none');
        let {routes} = this.props;

        let items = path.split("/");
        // console.log(items.length);
        let n = items.length;
        let pItems = ["/"];
        let pages = [];
        let last = "";
        for(let i=1; i < n; i++){
            // console.log(123);
            if(parseInt(items[i]) > 0)
             items[i] = ":id";

            last += "/"+items[i];
            pItems.push(last);
        }

        routes.forEach((r)=>{
            pItems.forEach((page, index)=>{
                if(r.path == page){
                    pages.push({path: page, label: r.label, subLabel: r.subLable});
                    delete pItems[index];
                }
            })
        })

        this.setState({pathAssets: pages, location: path});
        // console.log(pages);
    }

    render(){
        const {pathAssets, btnDisplay} = this.state;
        let count = pathAssets.length;
        if(count == 0) return(<div></div>);
        return(
            <div className="col-md-12">
                <div className="breadcrumb-box border shadow">
                    <ul className="breadcrumb">
                        {
                            pathAssets.map((path, index)=>
                                <li key={"bread"+index}>
                                    <Link key={"link"+index} to={path.path}> {path.subLabel} </Link>
                                </li>
                            )
                        }

                    </ul>
                    {/* <div className="breadcrumb-left">
                        سه شنبه، 1396/12/1 <i className="icon-calendar"></i>
                    </div> */}
                </div>
            </div>
        );
    }
}

export default BreadCrumb;
