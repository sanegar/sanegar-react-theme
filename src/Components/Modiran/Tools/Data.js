import axios from 'axios';

import Config from '../../Config';

import {NotificationManager} from 'react-notifications';



class Data{

    constructor(){

        // this.url = url;

    }



    /**

     *

     * @param {*} refs

     * @param {*} type = new or edit

     */

    static save(url, parent, type='new', nextUrl='',callback=''){

        let data = {};

        let {refs} = parent;



        let targetBTN = window.event.target;



        $('#contentFrame').css('filter', 'blur(1px)');

        $(targetBTN).addClass('btn-loading');

        $(targetBTN).attr('disabled', 'disabled');



        if(type=="edit"|| type=="update"){

            data['_method'] = 'PUT';

        }



        Object.keys(refs).forEach((ref, index)=>{

                if(ref.value!=undefined){

                    data[ref] = refs[ref].value;

                }

                else{

                    // console.log(refs[ref].refs.item.dataset.type);

                    if(refs[ref].refs.item.type == "checkbox"){

                        if(data[ref] = refs[ref].refs.item.checked)

                        {

                            data[ref] = refs[ref].refs.item.value;

                        }

                        else{

                            data[ref] = 0;

                        }

                    }

                    else if(refs[ref].refs.item.multiple == true)

                    {

                        // console.log(refs[ref].refs.item);

                        var values = window.$('#'+refs[ref].refs.item.id).val();

                        data[ref] = values;

                        // console.log(values);



                    }

                    else if(refs[ref].refs.item.dataset.type=='editor'){

                        // console.log(refs[ref].refs.item.editor);

                        let value = refs[ref].refs.item.editor.getData();

                        // console.log(value);

                        // let val = $("#"+refs[ref].refs.item.id).getData();

                        data[ref] = value;

                    }

                    else{

                        data[ref] = refs[ref].refs.item.value;

                    }

                    // if(refs[ref].refs.item.type != "editor")

                    // {

                    //     refs[ref].refs.item.value = editor.getData;

                    // }



                }

            }

        );

        axios.post(`${Config.getHost()}${url}`, data)


        // axios({

        //     headers: {

        //         // 'content-type': 'application/json',

        //         'Content-Type': 'application/x-www-form-urlencoded'

        //     },

        //     method: 'POST',

        //     url: `${Config.getHost()}${url}`,

        //     // data: data, methodNotAllowed

        //     params: data

        // })

        .then((response)=>{
            console.log(response)

            if(typeof callback == "string"){
                NotificationManager.success(response.data, 'پیام', 5000);
            }

            if(typeof callback == "function"){
                callback(response.data);
            }

            if(nextUrl != ''){
                parent.props.history.replace(nextUrl);
            }

        })

        .catch((error)=>{

            if (error.response) {

                // The request was made and the server responded with a status code

                // that falls out of the range of 2xx

                // console.log(error.response.data);

                // console.log(error.response.status);

                // console.log(error.response.headers);



                if(error.response.status == 422){

                    parent.setState({errors: error.response.data.errors});

                    NotificationManager.error('داده ها به درستی ارسال نگردیده است!!', 'پیغام خطا', 5000);

                }



                if(error.response.status == 401){

                    parent.setState({errors: error.response.data.errors});

                    NotificationManager.error('مجوز دسترسی شما به سایت منقضی شده است، لطفا مجدد وارد شوید!!', 'پیغام خطا', 5000);

                }



            } else if (error.request) {

                // The request was made but no response was received

                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of

                // http.ClientRequest in node.js

                // console.log(error.request);

            } else {

                // Something happened in setting up the request that triggered an Error

                console.log('Error', error.message);

            }

            // console.log(error.config);

            // alert(error);

            if(error == 'Error: Request failed with status code 422'){

                // NotificationManager.error('داده ها به درستی ارسال نگردیده است!!', 'پیغام خطا', 5000);

            }

            else{

                NotificationManager.error('خطا در اتصال', 'پیغام خطا', 5000);

            }

        })

        .then(()=>{

            setTimeout(()=>{

                $(targetBTN).removeClass('btn-loading');

                $(targetBTN).attr('disabled', false);

                $('#contentFrame').css('filter', 'blur(0)');

            }, 1000);

        });

    }



    static getInfo(url, parent, variableName = "items"){

        // console.log(parent);

        const options = {

            url:Config.getHost()+url,

            method: "GET",

            header: {

                'content-type': 'application/json'

            }

        }

        axios(options)

            .then((response)=>{

                // console.log("parent is: ");

                // console.log(parent.setState);

                parent.setState({[variableName]: response.data});

            })

            .catch((error)=>{

                // console.log(error);

                if (error.response) {

                    // console.log(error.response.data);

                    // console.log(error.response.status);



                    if(error.response.status == 422){

                        parent.setState({errors: error.response.data.errors});

                        NotificationManager.error('داده ها به درستی ارسال نگردیده است!!', 'پیغام خطا', 5000);

                    }

                    if(error.response.status == 401){

                        // parent.setState({errors: error.response.data.errors});

                        NotificationManager.error('مجوز دسترسی شما به سایت منقضی شده است، لطفا مجدد وارد شوید!!', 'پیغام خطا', 5000);

                        setTimeout(()=>{window.location.href = Config.basePath}, 3000);

                    }



                }

            })

            .then(()=>{

                setTimeout(()=>parent.setState({loading: ""}), 400);

            });

    }

    destroy(e){
        // let id = e.currentTarget.attributes['data-id'].value;
        // let url = `${Config.getHost()}/adminbrands/del/${id}`;
        let result = window.confirm("آیا از حذف مطمئن هستید؟");
        let url = `${Config.getHost()}`+e.currentTarget.attributes['data-url'].value;
        if(result == true){
            // alert('deleting record');
            const options = {
                url,
                method: "POST",
                header: {
                    'content-type': 'multipart/form-data'
                },
                data: {'_method':"delete"}
            }

            axios(options)
                .then((response)=>{
                    let rand = Math.random()*10000;
                    // NotificationManager.success('داده ی مورد نظر حذف گردید!!', 'پیغام', 5000);
                    NotificationManager.success(response.data, 'پیغام', 5000);
                    this.props.history.push(this.props.history.location.pathname+`?${rand}`);
                })
                .catch((error)=>{
                    if(error.response.status == 404){
                        // alert('رکورد مورد نظر یافت نشد، احتمالا قبلا حذف گردیده است!!');
                        NotificationManager.error('رکورد مورد نظر یافت نشد، احتمالا قبلا حذف گردیده است!!', 'پیغام خطا', 5000);

                        this.render();
                    }
                })
        }
        else{
            this.props.history.push(this.props.history.location.pathname+`?${rand}`);
        }
    }

    archive(e)
    {
        let result = window.confirm("آیا آگهی مورد نظر به لیست آرشیو منتقل شود؟");
        let url = `${Config.getHost()}`+e.currentTarget.attributes['data-url'].value;
        if(result == true){
            const options = {
                url,
                method: "POST",
                header: {
                    'content-type': 'multipart/form-data'
                }
            }
            axios(options)
            .then((response)=>{
                let rand = Math.random()*10000;
                // NotificationManager.success('داده ی مورد نظر حذف گردید!!', 'پیغام', 5000);
                NotificationManager.success(response.data, 'پیغام', 5000);
                this.props.history.push(this.props.history.location.pathname+`?${rand}`);
            })
            .catch((error)=>{
                if(error.response.status == 422){
                    alert('شما فقط میتوانید آگهی های تایید شده را آرشیو نمایید!');
                    this.render();
                }
            })
        }
    }
}



export {Data};

