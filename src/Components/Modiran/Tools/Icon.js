import React, { Component } from 'react';
import {Link} from 'react-router-dom';

/**
 * Icon Component
 * click:           onClick function
 * to:              The "to" attribute of Link Component
 * materialIcon:    The Material Icon name
 * iconClass:       The "i" class name used for icon
 * id:              The item id
 * displayIf:       The element display condition
 * style:           The "a" tag css styles
 * iconStyle:       The icon css styles
 */
class Icon extends Component{
    constructor(props){
        super(props);
    }
    render(){

        let {click, url, label, to,classLink, materialIcon, iconClass, id, displayIf, style, iconStyle, title, options,src,classImg,className} = this.props;
        // console.log(label);

        let content = '';

        if(label == undefined){

            if(materialIcon != undefined)
                content = <i className="material-icons" style={iconStyle}>{materialIcon}</i>;
            else if(iconClass != undefined)
                content = <i className={iconClass} style={iconStyle}></i>;
        }
        else if(label != undefined){
            content = label;
        }
        if(src!=undefined){
            content = <img src={src} className={classImg} />
        }
        // alert(content);



        if(displayIf == undefined) displayIf = "true";

        if(eval(displayIf))
            if(to != undefined)
                return <Link to={to} style={style} key={Math.random()*1000} className={(title!=undefined?'tooltip ':" ")+(classLink!=undefined?classLink:"") + className} alt={title} {...options}> {content} </Link>;
            else
                return <a style={{cursor:"pointer", ...style}} key={Math.random()*1000} className={(title!=undefined?'tooltip ':" ")+(classLink!=undefined?classLink:"") + className}  {...options} alt={title} onClick={(event)=>click(event)} data-url={url} data-id={id}> {content} </a>;
        else
            return <React.Fragment></React.Fragment>
    }
}

export {Icon};
