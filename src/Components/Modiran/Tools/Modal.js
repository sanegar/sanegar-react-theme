import React, {Component} from 'react';
import axios from 'axios';
// import Config from '../../Config';
// import {Tools, Data} from '../';

class Modal extends Component {
    constructor(props){
        super(props);
        this.state ={
            id: (props.id != undefined)?props.id:'exampleModal',
        };
        this.componentDidMount = this.componentDidMount.bind(this);
    }

    componentDidMount(){
        let {id} = this.state;
        window.$("#"+id+'Content').on("show.bs.modal", function(e) {
            var t = window.$(e.relatedTarget).data("whatever"),
                a = window.$(this);
            a.find(".modal-title").text("پیام جدید به " + t), a.find(".modal-body input").val(t)
        })
    }

    render(){
        let {id} = this.state;
        let {style, title, className, display, hideModal} = this.props;

        return(<React.Fragment>
                    <div className={"modal"} key={Math.random()*1000} style={{display:display?"block":"none"}} id={id} tabIndex="-1" role="dialog" aria-hidden="true" >                        <div className={"modal-dialog "+className} role="document" style={style}>
                            <div className="modal-content" style={{zIndex:1060}}>
                                <div className="modal-header">
                                    <h5 className="modal-title" id={id+"Label"}>{title}</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={hideModal}>
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body"> {this.props.children} </div>
                            </div>
                        </div>
                    </div>
                    <div className="modal shadow" onClick={hideModal} style={{display:display?"block":"none", backgroundColor:'#111', opacity:"0.65", zIndex:1040}}></div>
                </React.Fragment>
            );
    }
}

export {Modal};
