import React, {Component} from 'react';
import axios from 'axios';
import $ from 'jquery';
import JsxParser from 'react-jsx-parser';
import { NotificationManager } from 'react-notifications';
// import * as Babel from "@babel/core";
// import {transform} from "@babel/core";
// import ReactHtmlParser, {htmlparser2} from 'react-html-parser';
// import JSXParser from 'index';
import {Icon} from './Icon';
import {Pic} from './Pic';
import {Cookies} from './Cookies';
import {Paging} from './Paging';
import {Link} from 'react-router-dom';
import Config from '../../Config';
// import Paging from 'Components/Publics/Paging';
/**
 * Grid Class
 * Design By Mehdi Wosughi
 *
 * Props:
 *      url: the main grin url
 *      columns
 *      activeSort
 *      activeSearch
 *      insertLink
 *      insertLabel
 */
class Grid extends Component{
    constructor(props){
        super(props);

        this.getInfo = this.getInfo.bind(this);
        this.isJSX = this.isJSX.bind(this);
        this.search = this.search.bind(this);
        this.renderHeader = this.renderHeader.bind(this);
        this.setNewValues = this.setNewValues.bind(this);
        this.getLastValues = this.getLastValues.bind(this);

        const defaultValues = this.getLastValues();

        this.state = {
            url: props.url,
            items: [],
            hash: "",
            columns: props.columns!=undefined? props.columns:{},
            loading: 'tableLoading',
            page: defaultValues.page,
            pageInfo: {},
            status: "",
            perPage: defaultValues.perPage,

            sort: defaultValues.sort,
            sortLabel: defaultValues.sortLabel,
            sortType: defaultValues.sortType,
            sortTypeLabel: defaultValues.sortTypeLabel,
            activeSort: props.activeSort,
            search: defaultValues.search,
            activeSearch: props.activeSearch,
            insertLink: props.insertLink,
            insertLabel: props.insertLabel != undefined? props.insertLabel: "مورد جدید",
            multiView: props.multiView,
            filters: props.filters != undefined? props.filters: []
        };


        // evalJSX.globalNs = 'React';
    }

    getLastValues(){
        let name = this.props.url;
        let result = Cookies.get(name+"values");
        if(result == null || result == undefined){
            result = {page:1, perPage:5, sort:"", sortLabel:"", sortType:"", sortTypeLabel:"", search:""};
        }
        else{
            result = JSON.parse(result);
        }

        // console.log("default values: ");
        // console.log(result);

        return result;
    }

    setNewValues(){
        let name = this.props.url;
        let {page, perPage, sort, sortLabel, sortType, sortTypeLabel, search} = this.state;
        const state = {page, perPage, sort, sortLabel, sortType, sortTypeLabel, search};
        let result = JSON.stringify(state);
        // console.log(result);

        Cookies.set(name+"values", result);
    }

    componentWillMount() {
        // console.log("willMountHash: "+window.location.hash);
        this.state.hash = window.location.hash;
        this.getInfo(this.state.page);
    }

    componentWillUpdate(){
        // console.log("WillUpdate: "+window.location.hash);
        if(this.state.hash != window.location.hash){
            this.state.hash = window.location.hash;
            this.getInfo(this.state.page);
        }
        // console.log('update');
    }
    componentDidUpdate(){
        window.$('i:contains(edit)').addClass('editClass');
        window.$('i:contains(visibility)').addClass('viewClass');
        window.$('i:contains(close)').addClass('deleteClass');

    }

    getInfo(page=1){
        // console.log(this.state);
        this.setState({loading: "tableLoading"});
        let {url} = this.state;
        this.state.page = page;
        const options = {
            url,
            method: "GET",
            header: {
                'content-type': 'application/json'
            },
            params: {
                page: page,
                number: this.state.perPage,
                sort: this.state.sort,
                sortType: this.state.sortType,
                search: this.state.search
            }
        }

        this.setNewValues();

        axios(options)
            .then((response)=>{
                // console.log(response.data);
                let items = response.data.data;
                let pageInfo = response.data;

                if(items == undefined || items.length == 0) this.state.status = "no-data";
                // console.log(items.length);
                delete pageInfo.data;
                this.setState({
                    items,
                    pageInfo
                })
            })
            .catch((error)=>{
                // console.log(error);
                if (error.response) {
                    // console.log(error.response.data);
                    // console.log(error.response.status);

                    if(error.response.status == "422"){
                        parent.setState({errors: error.response.data.errors});
                        NotificationManager.error('داده ها به درستی ارسال نگردیده است!!', 'پیغام خطا', 5000);
                    }
                    else if(error.response.status == "401"){
                        // parent.setState({errors: error.response.data.errors});
                        // console.log(error.response.status+" Auth Error!!!");
                        // alert('لطفا مجدد وارد شوید!!');
                        // setTimeout(()=>NotificationManager.success('مجوز دسترسی شما به سایت منقضی شده است، لطفا مجدد وارد شوید!!', 'پیام'), 2600);
                        NotificationManager.error('مجوز دسترسی شما به سایت منقضی شده است، لطفا مجدد وارد شوید!!', 'پیغام خطا', 5000);
                        setTimeout(()=>{window.location.href = Config.basePath}, 3000);
                    }

                }
            })
            .then(()=>{
                setTimeout(()=> this.setState({loading: ""}), 400);
            });
    }

    resolve(path, obj) {
        return path.split('.').reduce(function(prev, curr) {
            return prev ? prev[curr] : null
        }, obj || self)
    }

    isJSX1(x){
        if(typeof x == "string"){
            return false;
        }
        else
        {
            return true;
        }
    }

    isJSX(x){
        if(x.substr(0, 1) == "<"){
            return true;
        }
        else
        {
            return false;
        }
    }

    search(e){
        let search = this.refs.search.value;
        let keyCode = e.which;
        if(keyCode==13){
            this.state.search = search;
            this.getInfo(1);
        }
    }

    proccessFilters(){
        this.props.filters.forEach((filter, index)=>{
            if(filter.url != "" && filter.url != undefined)
            {
                axios.get(filter.url)
                    .then((response)=>{
                        // console.log(response.data);
                        let data = [];
                        response.data.forEach((d)=>{
                            if(filter.valueIdex == "" && filter.valueIdex == undefined) filter.valueIdex = "id";
                            if(filter.labelIndex == "" && filter.labelIndex == undefined) filter.labelIndex = "title";
                            data.push({value: d[filter.valueIdex], label: d[filter.labelIndex]})
                        });
                        this.props.filter[index].data = response.data;
                    })
                    .catch((error)=>{
                        console.log("Filter Error:"+this.props.filter[index].name+"-"+error);
                    });
            }
        });
    }

    toJSX(str, items){
        return <JsxParser
            bindings={items}
            components={{ Link, Config, Icon, Pic }}
            jsx={str}
        />
    }

    toJSX0(Str){
        // var JSXCode = Babel.transform(Str, {presets: ["react"]}).code;
        // let JSXCode = transform(Str, {presets: ["react"]}).code;
        // return <div>{eval(JSXCode)}</div>;
    }

    render(){
        let {columns, pageInfo, items, loading, insertLink, insertLabel, status} = this.state;
        // console.log(items);

        if(insertLink != undefined){
            $('#newDIV').css("display", "inline");
            $('#newBTN').attr("href", insertLink);

            if(insertLabel){
                $('#newBTN').html(insertLabel);
            }
        }

        return(
            <div id='grid'>
                {this.renderHeader()}
                <div className="separator mb-5"></div>
                <div className="table-responsive">
                    <table className={"table table-striped "+loading}
                        role="grid" id="grid">
                        <thead>
                            <tr>
                                <th> ردیف </th>
                                {columns.map((col, index)=>
                                    <th key={index} style={{width:col.width}}>{col.label}</th>
                                )}
                            </tr>
                        </thead>
                        <tbody>
                        {(()=>{
                            if(status == "no-data")
                                return <tr><td colspan={columns.length+1} style={{textAlign:"center"}}> هیچ داده ای جهت نمایش وجود ندارد </td></tr>
                            else
                                return items.map((item, index)=>
                                    <tr key={index} role="row1" className={(index%2==0)?"even":"odd"}>
                                        <td key='1000'>
                                            {pageInfo.from+index}
                                        </td>

                                        {columns.map((col, index)=>{
                                            if(!this.isJSX(col.field)){
                                                let resolve = this.resolve(col.field, item);
                                                let result = [];
                                                // console.log();
                                                // console.log(typeof resolve + resolve);
                                                if(typeof resolve != "object" || resolve == null){
                                                    result.push(<span dangerouslySetInnerHTML={{__html: resolve}} />);
                                                }
                                                else{
                                                    resolve.map((item)=>{
                                                        result.push(<div>
                                                            {item.title?item.title
                                                                :item.name?item.name
                                                                :item.label?item.label:""}
                                                            </div>);
                                                    })
                                                }
                                                return (<td key={index}>
                                                    {/* {this.resolve(columns[col], item)}  */}
                                                    {/* {this.toJSX("{item."+col.field+"}", {this:this, item:item})} */}
                                                    {result}
                                                </td>)
                                            }
                                            else
                                            {
                                                // console.log(col.field);
                                                return (<td key={index}>
                                                    {/* <div dangerouslySetInnerHTML={{__html: columns[col].replace(/:id/g, item.id)}}></div> */}
                                                    {this.toJSX(col.field, {this:this, item:item, ...col.bindings})}
                                                </td>)
                                            }
                                        }
                                    )}
                                    </tr>
                            )})()
                        }
                        </tbody>
                    </table>
                </div>
                {
                    (pageInfo.last_page > 2) ? <div className="separator mb-2"></div> : ""
                }
                <Paging pageInfo={pageInfo} pagingHandler={this.getInfo} />
            </div>
        );
    }

    renderHeader(){

        let {pageInfo, perPage, activeSearch, activeSort, multiView, columns, sort, sortLabel, sortType, sortTypeLabel, filters} = this.state;
        const numbers = [5, 10, 20, 30, 50, 100];

        return(
            <div className="d-md-block" id="displayOptions">
                {(multiView)?
                    <span className="ml-3 mb-2 d-inline-block float-md-right">
                        <a href="#" className="mr-2 active">
                            <i className="flaticon-view-5 view-icon"></i>
                        </a>
                        <a href="#" className="mr-2">
                            <i className="flaticon-view-4 view-icon"></i>
                        </a>
                        <a href="#" className="mr-2">
                            <i className="flaticon-view-1 view-icon s"></i>
                        </a>
                    </span>
                    :""
                }
                <div className="d-block d-md-inline-block">
                    {
                        filters.map((filter, index)=>
                            <div key={index} className="btn-group float-md-left mr-1 mb-1">
                                <button className="btn btn-outline-dark btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {filter.label}
                                </button>
                                <div className="dropdown-menu dropdown-menu-right">
                                    {
                                        filter.data.map((item, index)=>
                                            <li>
                                                <a key={index} className={this.state.sortType == "ASC"?"dropdown-item active":"dropdown-item"}
                                                    onClick={()=>{
                                                        this.state.sortType = "ASC";
                                                        this.state.sortTypeLabel = "صعودی";
                                                        this.getInfo(1);
                                                    }}>
                                                    {item.label}
                                                </a>
                                            </li>
                                        )
                                    }
                                </div>
                            </div>
                        )
                    }
                    {(activeSearch)?
                        <div className="search-sm d-inline-block float-md-right mr-1 mb-1 align-top" onMouseDown={this.search}>
                            <input placeholder="جستجو..." ref="search" defaultValue={this.state.search} onKeyUp={this.search} />
                        </div>
                        :""
                    }
                    {(activeSort)?
                        <React.Fragment>
                        <div className="btn-group float-md-right mr-1 mb-1">
                            <button className="btn btn-outline-dark btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {(sortLabel!="")?sortLabel:"ترتیب بر اساس"}
                            </button>
                            <div className="dropdown-menu dropdown-menu-right" x-placement="bottom-start">
                                <li key={200000}>
                                    <a onClick={()=>{
                                            this.setState({sort: "", sortLabel: ""});
                                            this.getInfo(1);
                                        }}
                                        className={this.state.sort == ""?"dropdown-item active":"dropdown-item"}>فیلد پیشفرض</a>
                                </li>

                                {columns.map((item, index)=>{
                                        if(item.sort)
                                            return <li key={index}>
                                                        <a onClick={()=>{
                                                                this.setState({sort: item.field, sortLabel: item.label});
                                                                this.getInfo(1);
                                                            }}
                                                            key={index}
                                                            className={this.state.sort == item.field?"dropdown-item active":"dropdown-item"}>{item.label}</a>
                                                    </li>
                                        else
                                            return null;
                                    }
                                )}
                            </div>
                        </div>
                        <div className="btn-group float-md-right mr-1 mb-1">
                            <button className="btn btn-outline-dark btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {(sortTypeLabel!="")?sortTypeLabel:"ترتیب"}
                            </button>
                            <div className="dropdown-menu dropdown-menu-right">
                                <li key={"a1"}><a className={this.state.sortType == "ASC"?"dropdown-item active":"dropdown-item"} onClick={()=>{
                                        this.state.sortType = "ASC";
                                        this.state.sortTypeLabel = "صعودی";
                                        this.getInfo(1);
                                    }}>صعودی</a>
                                </li>
                                <li key={'a2'}><a className={this.state.sortType == "DESC"?"dropdown-item active":"dropdown-item"} onClick={()=>{
                                        this.state.sortType = "DESC";
                                        this.state.sortTypeLabel = "نزولی";
                                        this.getInfo(1);
                                    }}>نزولی</a>
                                </li>
                            </div>
                        </div>
                        </React.Fragment>
                        :""
                    }

                </div>
                <div className="float-md-left btn-group">
                    <button className="btn btn-outline-dark btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {perPage}
                    </button>
                    <div className="dropdown-menu dropdown-menu-right">
                        {numbers.map((n, i)=>
                            <li key={"li"+i}>
                                <a key={i} className={n!=perPage?"dropdown-item":"dropdown-item active"}
                                onClick={()=>{ this.state.perPage = n; this.getInfo(1) }}>{n}</a>
                            </li>
                        )}
                    </div>
                    <span className="text-muted text-small">نمایش {pageInfo.from}-{pageInfo.to} از {pageInfo.total} آیتم </span>
                </div>
            </div>
        );
    }
}

export {Grid};
export default Grid;
