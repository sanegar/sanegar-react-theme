import React, {Component} from 'react';

class NoLabel extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div className="card mb-4" id="contentFrame">
                <div className="card-body">
                    {
                        this.props.children
                    }
                </div>
            </div>
        );
    }
}

export {NoLabel};
