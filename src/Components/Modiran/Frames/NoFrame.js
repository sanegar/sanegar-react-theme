import React, {Component} from 'react';

class NoFrame extends Component{
    constructor(props){
        super(props);
    }

    render(){
        let {className} = this.props;
        let key = "box"+Math.random()*100;
        if(className == undefined) className = "col-md-12";
        return(
            <div className={"key "+className} key={key}>
                <div className="portlet box border shadow">
                    <div className="portlet-body">
                        <div className="row">
                            <div className="col-md-12">
                                {
                                    this.props.children
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        // </div>

        );
    }

    componentDidMount(){
        // console.log(window.Modiran);
    }
}

export {NoFrame};
