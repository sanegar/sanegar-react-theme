import React, {Component} from 'react';

class BaseFrame extends Component{
    constructor(props){
        super(props);
        this.state = {
            key: "box"+Math.random()*100
        }
    }

    render(){
        let {className} = this.props;
        let {key} = this.state;
        if(className == undefined) className = "col-md-12";
        return(
            <div className={"key "+className}>
                <div className="portlet box border shadow">
                    <div className="portlet-heading">
                        <div className="portlet-title">
                            <h3 className="title">
                                <i className="icon-note"></i> {this.props.label}
                            </h3>
                        </div>
                        <div className="buttons-box">
                            <a className="btn btn-sm btn-default btn-round btn-fullscreen"
                                rel="tooltip" title="تمام صفحه"
                                onClick={window.Modiran?window.Modiran.boxFullScreen:null}>
                                <i className="icon-size-fullscreen"></i>
                            </a>
                            <a className="btn btn-sm btn-default btn-round btn-collapse" rel="tooltip" title="کوچک کردن" href="#">
                                <i className="icon-arrow-up"></i>
                            </a>
                        </div>
                    </div>
                    <div className="portlet-body">
                        <div className="row">
                            <div className="col-md-12">
                                {
                                    this.props.children
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        // </div>

        );
    }

    componentDidMount(){
        // console.log(window.Modiran);
    }
}

export {BaseFrame};
