import React, {Component} from 'react';

export class Setting extends Component{
    render(){
        return(
            <React.Fragment>
                <div className="settings hidden-xs">
                <a href="#" className="btn" id="toggle-setting">
                    <i className="icon-settings"></i>
                </a>
                <h3 className="text-center">تنظیمات</h3>
                
                <div className="fix-header-box">
                    <p className="h6">
                        هدر ثابت: 
                        <span className="pull-left">
                            <input type="checkbox" className="fix-header-switch normal" defaultChecked />
                        </span>
                    </p>
                </div>
                <hr className="light" />
                <div className="toggle-sidebar-box">
                    <p className="h6">
                        جمع کردن سایدبار: 
                        <span className="pull-left">
                            <input type="checkbox" className="toggle-sidebar-switch normal" />
                        </span>
                    </p>
                </div>
                <hr className="light" />
                <div className="toggle-sidebar-box">
                    <p className="h6">
                        سایدبار خلاقانه: 
                        <span className="pull-left">
                            <input type="checkbox" className="creative-sidebar-switch normal" />
                        </span>
                    </p>
                </div>
                <hr className="light" />
                <div className="theme-colors">
                    <p className="h6">رنگ قالب : </p>
                    <a className="btn btn-round btn-blue ripple-effect active" data-color="blue"></a>
                    <a className="btn btn-round btn-red ripple-effect" data-color="red"></a>
                    <a className="btn btn-round btn-green ripple-effect" data-color="green"></a>
                    <a className="btn btn-round btn-orange ripple-effect" data-color="orange"></a>
                    <a className="btn btn-round btn-darkblue ripple-effect" data-color="darkblue"></a>
                    <a className="btn btn-round btn-purple ripple-effect" data-color="purple"></a>
                </div>
                <div className="theme-code ltr text-left">
                    <code></code>
                </div>
            </div>
            
            
            <div className="modal fade" id="code-modal" role="dialog">
                <div className="modal-dialog modal-lg">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="btn btn-default btn-round btn-icon pull-right" id="btn-copy"><i className="fa fa-copy"></i></button>
                            <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 className="modal-title">کپی کردن کدها</h4>
                        </div>
                        <div className="modal-body"></div>
                    </div>
                </div>
            </div>        
        </React.Fragment>
        );
    }
}