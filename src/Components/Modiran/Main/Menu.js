import React, {Component} from 'react';
// import {menus} from './../../menus';
import {Link} from 'react-router-dom';
// import {loadThemeStyle, themeLoad} from './js';

class Menu extends Component{
    constructor(props){
        super(props);
        this.state = {user: window.userInfo};
    }

    componentDidMount() {
        // document.getElementById('startApp').value = "start"
    }


    render(){
        let user = this.state.user;
        let {menus,path} = this.props;

        return(
            <div id="sidebar">
                <div className="sidebar-top">
                    {/* <div className="search-box">
                        <form className="search-form" action="#" method="GET">
                            <div className="input-group">
                                <span className="input-group-btn">
                                    <button type="submit" className="btn btn-round submit">
                                        <i className="icon-magnifier"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div> */}
                    <div className="user-box">
                        <a href="#">
                            <img src={"/media/users/avatar/"+(user.pic?user.pic:"profileDefault.png")} alt="عکس پروفایل" className="img-circle img-responsive" />
                        </a>
                        <div className="user-details">
                            <h4> {user.name} </h4>
                            <p className="role"> {user.brand} </p>
                            {/* <div className="dropdown user-login">
                                <button className="btn btn-xs btn-status dropdown-toggle btn-round" type="button" data-toggle="dropdown" data-hover="dropdown">
                                    <i className="fa fa-circle text-success"></i>
                                    <span>دردسترس</span>
                                </button>
                                <ul className="dropdown-menu dropdown-status">
                                    <li>
                                        <a href="#" className="busy">
                                            <i className="fa fa-circle text-success"></i>
                                            <span>دردسترس</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" className="busy">
                                            <i className="fa fa-circle text-danger"></i>
                                            <span>مشغول</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i className="fa fa-circle text-gray"></i>
                                            <span>مخفی</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i className="fa fa-circle text-warning"></i>
                                            <span>سایر</span>
                                        </a>
                                    </li>
                                </ul>
                            </div> */}
                        </div>
                    </div>
                </div>
                <div className="side-menu-container">
                    <ul className="metismenu" id="side-menu">
                        {
                            menus.map((menu, index)=>
                                    <li key={'parent'+index}>
                                        <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                                            <i className={menu.icon?menu.icon:"icon-drop"}></i>
                                            <span>{menu.label}</span>
                                        </a>
                                        <ul>
                                        {
                                            menu.childs.map((cmenu, index)=>
                                            <li key={index}>
                                                <Link to={ cmenu.path } className="">
                                                    <i className={cmenu.icon?cmenu.icon:"icon-drop"}></i>&nbsp;
                                                    <span>{ cmenu.label }</span>
                                                </Link>
                                            </li>
                                            )
                                        }
                                        </ul>
                                    </li>

                                )
                        }
                    </ul>
                </div>
            </div>
            // </div>
        );
    }
}

export {Menu};
