import React, {Component} from 'react';
import {Route} from 'react-router-dom';
// import {routes} from '../../routes';
import BreadCrumb from '../Tools/BreadCrumb';
import { Footer } from './Footer';
import { BaseFrame } from '../Frames/BaseFrame';

class Routes extends Component{
    render(){
        // let {routes, Components} = this.props;
        let {routes} = this.props;
        return(
            <div id="page-content">
                <div className="row">
                    <Route path='/' routes={routes} component={BreadCrumb} />
                    {
                        routes.map((route, index)=>
                            <Route
                                path={route.path}
                                key={index}
                                component={route.component}
                                exact={route.exact} />
                        )
                    }
                </div>
                <Footer />
            </div>
        );
    }
    render1(){
        return(
            <main>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <Route path="/:path" component={BreadCrumb} />
                            {
                                routes.map((route, index)=>
                                    <Route
                                        path={route.path}
                                        key={index}
                                        component={route.component}
                                        exact={route.exact} />
                                )
                            }
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

export {Routes};
