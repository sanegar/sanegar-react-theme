import React, {Component} from 'react';

export class Footer extends Component{
    render(){
        return(
            <div className="row footer-container">
                <div className="col-md-12">
                    <div className="copyright">
                        <p className="pull-right">
                            طراحی شده توسط تیم نرم افزاری رایاکس
                        </p>
                        <p className="pull-left ltr tahoma">
                            <span>©</span>
                            <a href="http://rayax.ir">رایاکس</a>
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}