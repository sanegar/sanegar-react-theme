import React, {Component} from 'react';
import Config from '../../Config';
import {Link} from 'react-router-dom';

class Header extends Component{
    constructor(props){
        super(props);
        // console.log(window.userInfo);
        this.state = {user: window.userInfo};
    }

    render(){
        let user = this.state.user;
        return(
            <div className="navbar navbar-fixed-top" id="main-navbar">
                <div className="header-right">
                    <a href={Config.getHost()+Config.basePath} className="logo-con">
                        <img src="/images/favicon.png" className="img-responsive center-block" alt="لوگو دووک" />
                    </a>
                </div>
                <div className="header-left">
                    <div className="top-bar">
                        <ul className="nav navbar-nav navbar-right">
                            <li>
                                <a href="#" className="btn" id="toggle-sidebar">
                                    <span></span>
                                </a>
                            </li>
                            <li>
                                <a href="#" className="btn open" id="toggle-sidebar-top">
                                    <i className="icon-user-following"></i>
                                </a>
                            </li>
                        </ul>
                        <ul className="nav navbar-nav navbar-left">
                            <li className="dropdown">
                                <a href="#" className="btn" id="toggle-fullscreen">
                                    <i className="icon-size-fullscreen"></i>
                                </a>
                            </li>
                            <li className="dropdown" style={{"display":"none"}}>
                                <a href="#" className="dropdown-toggle btn" data-toggle="dropdown">
                                    <i className="icon-envelope"></i>
                                    <span className="badge badge-primary">
                                        4
                                    </span>
                                </a>
                                <ul className="dropdown-menu has-scrollbar">
                                    <li className="dropdown-header clearfix">
                                        <span className="pull-right">
                                            <a href="#" rel="tooltip" title="خواندن همه" data-placement="left">
                                                <i className="icon-eye"></i>
                                            </a>
                                            شما 1 پیام تازه دارید.
                                        </span>
                                    </li>
                                    <li className="dropdown-body">
                                        <ul className="dropdown-menu-list" >
                                            <li className="clearfix">
                                                <a href="#">
                                                    <p className="clearfix">
                                                        <strong className="pull-right">
                                                            <img src={"/media/users/avatar/"+user.pic?user.pic:"profile.png"} className="img-circle" alt="" />
                                                            سهراب سپهری
                                                        </strong>
                                                        <small className="pull-left text-muted">
                                                            <i className="icon-clock"></i>
                                                            پانزده دقیقه پیش
                                                        </small>
                                                    </p>
                                                    <p>پیام پرمهرتان دریافت شد!</p>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li className="dropdown-footer clearfix">
                                        <a href="#">
                                            <i className="icon-list fa-flip-horizontal"></i>
                                            مشاهده همه پیام ها
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li className="dropdown" style={{"display":"none"}}>
                                <a href="#" className="dropdown-toggle btn" data-toggle="dropdown">
                                    <i className="icon-bell"></i>
                                    <span className="badge badge-warning">
                                        5
                                    </span>
                                </a>
                                <ul className="dropdown-menu has-scrollbar">
                                    <li className="dropdown-header clearfix">
                                        <span className="pull-right">
                                            <a href="#" rel="tooltip" title="خواندن همه" data-placement="left">
                                                <i className="icon-eye"></i>
                                            </a>
                                            <span>
                                                شما 8 اعلان تازه دارید.
                                            </span>
                                        </span>

                                    </li>
                                    <li className="dropdown-body">
                                        <ul className="dropdown-menu-list" >
                                            <li className="clearfix">
                                                <a href="#">
                                                    <p className="clearfix">
                                                        <strong className="pull-right">عباس دوران</strong>
                                                        <small className="pull-left text-muted">
                                                            <i className="icon-clock"></i>
                                                            21:30
                                                        </small>
                                                    </p>
                                                    <p>بسته ارسالی شما به دستم رسید.</p>
                                                </a>
                                            </li>
                                            <li className="clearfix">
                                                <a href="#">
                                                    <p className="clearfix">
                                                        <strong className="pull-right">حسن باقری</strong>
                                                        <small className="pull-left text-muted">
                                                            <i className="icon-clock"></i>
                                                            20:20
                                                        </small>
                                                    </p>
                                                    <p>از محبت شما ممنونم.</p>
                                                </a>
                                            </li>
                                            <li className="clearfix">
                                                <a href="#">
                                                    <p className="clearfix">
                                                        <strong className="pull-right">مدیر کل</strong>
                                                        <small className="pull-left text-muted">
                                                            <i className="icon-clock"></i>
                                                            19:20
                                                        </small>
                                                    </p>
                                                    <p>سفارش شما ارسال گردید..</p>
                                                </a>
                                            </li>
                                            <li className="clearfix">
                                                <a href="#">
                                                    <p className="clearfix">
                                                        <strong className="pull-right">مدیر مالی</strong>
                                                        <small className="pull-left text-muted">
                                                            <i className="icon-clock"></i>
                                                            17:40
                                                        </small>
                                                    </p>
                                                    <p>درخواست فیش حقوقی</p>
                                                </a>
                                            </li>
                                            <li className="clearfix">
                                                <a href="#">
                                                    <p className="clearfix">
                                                        <strong className="pull-right">ابراهیم همت</strong>
                                                        <small className="pull-left text-muted">
                                                            <i className="icon-clock"></i>
                                                            15:45
                                                        </small>
                                                    </p>
                                                    <p>پیام های مرا دنبال کنید.</p>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li className="dropdown-footer clearfix">
                                        <a href="#">
                                            <i className="icon-list fa-flip-horizontal"></i>
                                            مشاهده همه اعلانات
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li className="dropdown dropdown-user">
                                <a href="#" className="dropdown-toggle dropdown-hover" data-toggle="dropdown">
                                    <img src={"/media/users/avatar/"+(user.pic?user.pic:"profileDefault.png")} alt="عکس پرفایل" className="img-circle img-responsive header-logo" />
                                    <span> {user.name} </span>
                                    <i className="icon-arrow-down"></i>
                                </a>
                                <ul className="dropdown-menu">
                                    <li>
                                        <Link to="/profileEdit">
                                            <i className="icon-note"></i>
                                            ویرایش پروفایل
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/changePass" >
                                        <i className="icon-key"></i>
                                            تفییر رمز عبور
                                        </Link>
                                    </li>
                                    <li className="divider"></li>
                                    {/* <li>
                                        <a href="chat.html">
                                            <span className="badge badge-primary pull-left"> 14 </span>
                                            <i className="icon-envelope"></i>
                                            تیکت های جدید
                                        </a>
                                    </li> */}
                                    {/* <li>
                                        <a href="#">
                                            <i className="icon-wallet"></i>
                                            کیف پول
                                        </a>
                                    </li> */}
                                    <li>
                                        <a href="#" onClick={()=>window.location = "/social/"}>
                                            <i className="fa fa-instagram"></i>
                                            دووک نت
                                        </a>
                                    </li>
                                    <li className="divider"></li>
                                    <li>
                                        <a href="#" onClick={()=>window.document.getElementById('logout-form').submit()}>
                                            <i className="icon-power"></i>
                                            خروج
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export {Header};
