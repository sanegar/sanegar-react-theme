import React, {Component} from 'react';
import {Element} from './Element';

class Select2 extends Element{
    constructor(props){
        super(props);
        this.state = {id: ""};

        this.createId = this.createId.bind(this);
        this.createSelect2 = this.createSelect2.bind(this);
    }

    componentDidMount(){
        this.createSelect2();
    }

    componentDidUpdate(){
        this.createSelect2();
    }

    createId(){
        let id = "select"+(new Date()).getTime();
        return this.state.id = id;
    }

    createSelect2(){
        let {label, defaultValue, multiple, className, error, placeholder, options} = this.props;
        // let id = "select"+(new Date()).getTime() + Math.ceil(Math.round()*1000);
        let id = this.state.id;

        if( typeof defaultValue != "number" && typeof defaultValue != "string" && defaultValue != undefined && defaultValue.length > 0){
            let temp = [];
            defaultValue.map((item)=>{
                temp.push(item.id);
            });
            defaultValue = temp;
        }

        // alert(id);
        window.$(document).ready(function() {
            window.$(`#${id}`).select2({
                width: '100%',
                theme: "bootstrap",
                rtl: true,
                allowClear: true,
                placeholder: placeholder,
                maximumSelectionSize: 6
            })
            .on("select2:select",(e)=>{
                if (options!=undefined) {
                    options.onChange(e)
                }
            });
            window.$(`#${id}`).val(defaultValue).trigger("change");
        })
        // return id;

    }

    // renderWithIcon(){

    render(){

        let {icon} = this.props;
        this.createId();
        let {id} = this.state;
        // let id = this.createSelect2();


        if(icon == undefined){
            return this.renderSimple(id);
        }
        else{
            return this.renderWithIcon(id);
        }
    }

    renderWithIcon(id){
        let {label, multiple, className, error, options, alt, required, help, defaultValue} = this.props;
        let rand = Math.random()*1000;
        let divError = [];
        let helpDiv = '';
        let requiredDiv = '';

        if(required == "true")
        {
            requiredDiv = <sup className="required text-danger"> * </sup>;
        }
        if(help != undefined)
        {
            // helpDiv = <div className='alert alert-primary'> {help}</div>;
            helpDiv = <a href='#'  alt={help}><i class="fa fa-question-circle" aria-hidden="true"></i></a>;
        }
        if(error!=undefined){
            error.map((err, index)=>{
                divError.push(<div className='alert alert-danger' id={'error-'+rand} key={index} style={{display:"block"}}>{err}</div>);
            })
        }

        return(

            <div className={"form-group curve "+className}>
                <label>
                    {requiredDiv}
                    {label}
                    {helpDiv}
                </label>
                <div className="input-group">
                    <span className="input-group-addon">
                        <i className="icon-options"></i>
                    </span>
                    <select className="form-control select2 select2-hidden-accessible"
                        name = {name}
                        id = {id}
                        ref="item"
                        alt = {alt}
                        required = {required}
                        multiple={multiple}
                        defaultValue={defaultValue}
                        data ={(options!=undefined)?options.data:""}
                        >
                        {
                            this.props.children
                        }
                    </select>
                </div>
                {divError}
            </div>
        );
    }

    renderSimple(id){
        let {label, multiple, className, error, options, required, help, defaultValue} = this.props;
        let rand = Math.random()*1000;
        let divError = [];
        let helpDiv = '';
        let requiredDiv = '';

        if(required == "true")
        {
            requiredDiv = <sup className="required text-danger"> * </sup>;
        }
        if(help != undefined)
        {
            // helpDiv = <div className='alert alert-primary'> {help}</div>;
            helpDiv = <a href='#'  alt={help}><i class="fa fa-question-circle" aria-hidden="true"></i></a>;
        }
        // console.log(error)

        if(error!=undefined){
            error.map((err, index)=>{
                divError.push(<div className='alert alert-danger' id={'error-'+rand} key={index} style={{display:"block"}}>{err}</div>);
            })
        }

        return(

            <div className={"form-group curve "+className}>
                <label>
                    {requiredDiv}
                    {label}
                    {helpDiv}
                </label>

                <select className="form-control select2 select2-hidden-accessible"
                    name = {name}
                    id = {id}
                    ref="item"
                    multiple={multiple}
                    defaultValue={defaultValue}
                    data ={(options!=undefined)?options.data:""}
                    >
                    {
                        this.props.children
                    }
                </select>
                {divError}
            </div>
        );
    }
}



export {Select2};

