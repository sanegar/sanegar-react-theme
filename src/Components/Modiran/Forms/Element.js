import React, { Component } from 'react';
// import jsxToString from 'jsx-to-string';
import isEqual from "react-fast-compare";

class Element extends Component{
    constructor(props){
        super(props);
    }

    shouldComponentUpdate(nextProps){
        let changed = false;
        let changedProps = [];
        // console.log('shouldComponentUpdate:');
        let propsNeedsTocheck = ['defaultValue', 'label', 'children', 'error', 'className', 'data'];

        propsNeedsTocheck.forEach((key)=>{
            if(typeof nextProps[key] != typeof this.props[key]) {
                changed = true;
                changedProps.push(key);
            }
            else if(nextProps[key] instanceof Array){

                // console.log(key+": ");
                // console.log(this.props[key]);
                if(nextProps[key].length != this.props[key].length){
                    changed = true;
                    changedProps.push(key);
                }
                else if(key == "children" && nextProps.children != undefined){
                    if(!isEqual(nextProps.children, this.props.children)){
                        changed = true;
                        changedProps.push(key);
                    }
                }
                else if(this.props[key] != undefined){
                    nextProps[key].forEach((item, index)=>{
                        if(!isEqual(item, this.props[key][index])){
                        // if(JSON.stringify(item) != JSON.stringify(this.props[key][index])){
                            changed = true;
                            changedProps.push(key);
                        }
                    });
                }
            }
            else if(nextProps[key] instanceof Object){
                let str1 = JSON.stringify(nextProps[key]);
                let str2 = JSON.stringify(this.props[key]);

                if(str1 != str2){
                    changed = true;
                    changedProps.push(key);
                }
            }
            else{
                if(nextProps[key] != this.props[key]){
                    changed = true;
                    changedProps.push(key);
                }
            }
        });

        return(changed);
    }

    // componentDidMount(){
    //     if(this.refs.item){
    //         // console.log(this.refs.item);
    //         this.refs.item.onClick = ()=>alert(123);
    //     }
    // }

    checkError(){
        let {error} = this.props;

        let rand = Math.random()*1000;
        let divError = [];
        if(error!=undefined){
            error.map((err, index)=>{
                divError.push(<div className='alert alert-danger' id={'error-'+rand} key={index} style={{display:"block"}}>{err}</div>);
            })
        }
        return divError;
    }

    render(){
        return <div>Parent Element!!</div>
    }
}

export {Element};
