import React, {Component} from 'react';
import {Element} from './Element';

export class RadioBtn extends Element {
    constructor(props){
        super(props);

        this.state = {checkDefaultValue: true};
        this.setDefaultValue = this.setDefaultValue.bind(this);
    }

    setDefaultValue(){
        let {name, defaultValue} = this.props;
        let radios = document.getElementsByName(name);
        radios.forEach((item)=>{
            if (item.value == defaultValue){
                document.getElementById(item.id).checked = 'checked';
            }
        });
    }

    componentDidMount(){
        this.setDefaultValue();
    }

    componentWillReceiveProps(props){
        this.setDefaultValue();
    }

    render(){

        let {label, name, data, defaultValue, classNameDiv, classLabel, classIcon, onChange} = this.props;
        let {checkDefaultValue} = this.state;
        // if(onChange == undefined) onChange =
        let rand = Math.random(1000, 9999);
        let divError = [];

        // if(error!=undefined){
        //     error.map((err, index)=>{
        //         divError.push(<div className='alert alert-danger' id={'error-'+rand} key={index} style={{display:"block"}}>{err}</div>);
        //     })
        // }

        // console.log(name +' defaultValue:');
        // console.log(defaultValue);

        return(<div className={classNameDiv}>
                {/* <h6> <i className="glyph-icon fas fa-angle-left"></i>{label}</h6> */}
                <h6> <i className={"glyph-icon "+ classIcon}></i>{label}</h6>
                <div className="mb-4 pr-5">
                    {
                        data.map((item, index)=>
                            <div className={"custom-control custom-radio "+item.className +" "+ item.classBtn1+" "+ item.classBtn2} key={index}>
                                {
                                    <input  type="radio"
                                                key={index}
                                                ref={'item'+index}
                                                value={item.value}
                                                id={name+index}
                                                name={name}
                                                className={"custom-control-input "}
                                                onChange={onChange}
                                        />
                                }
                                <label className={"custom-control-label "+classLabel} htmlFor={name+index}>{item.label}
                                </label>
                            </div>
                        )
                    }

                </div>
            </div>

            );
    }
}
