import React, {Component} from 'react';
import {Element} from './Element';


export class DatePicker extends Element {
    constructor(props){
        super(props);
    }

    componentDidMount(){

        setTimeout(()=>
            window.$('.datepicker').persianDatepicker({
                format: 'YYYY/MM/DD',
                inline: false,
                initialValue: false
            })
        , 1000);
    }

    render(){
        let {name, label, className, defaultValue, error, placeholder, options} = this.props;
        let rand = Math.random()*10000;
        let divError = [];
        if(error!=undefined){
            error.map((err, index)=>{
                divError.push(<div className='alert alert-danger' id={'error-'+rand} key={index} style={{display:"block"}}>{err}</div>);
            })
        }

        return(
            <React.Fragment>
                <div className={"form-group "+className}>
                    <label>{label}</label>
                    <div className="input-group">
                        <span className="input-group-addon">
                            <i className="icon-calendar"></i>
                        </span>
                        <input className="form-control datepicker"
                                key={rand}
                            defaultValue = {defaultValue}
                            // value = {defaultValue}
                            ref='item'
                            name={name}
                            placeholder = {placeholder}
                            {...options}
                        />
                    </div>

                        {divError}

                </div>
            </React.Fragment>
        );
    }
}
