import React from 'react';

export const Button = (props) => {
    let {label, onClick, className, icon} = props;
    if (className == undefined) className = 'btn-info';
    if(icon == undefined) icon = "icon-check";

    return(
        <button type="submit" className={"btn btn-round "+className} onClick={(event)=>{
                window.event = event;
                onClick(event)}
            }>
            <i className={icon}></i>
            &nbsp; {label}
            <span className="loading"></span>
            <div className="paper-ripple">
                <div className="paper-ripple__background"></div>
                <div className="paper-ripple__waves"></div>
            </div>
        </button>
    );
}
