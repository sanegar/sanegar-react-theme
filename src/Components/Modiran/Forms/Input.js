import React, {Component} from 'react';
import {Element} from './Element';

export class Input extends Element {
    constructor(props){
        super(props);
        this.state = {divError:[]};
    }

    render(){
        let {icon, error} = this.props;
        let rand = Math.random(1000, 9999);
        let divError = [];
        if(error!=undefined){
            error.map((err, index)=>{
                divError.push(<div className='alert alert-danger' id={'error-'+rand} key={index} style={{display:"block"}}>{err}</div>);
            })
        }
        this.state.divError = divError;

        if(icon == undefined)
            return this.renderSimple();
        else
            return this.renderWithIcon();
    }

    renderWithIcon(){
        let {label, defaultValue, icon, className, type,placeholder, options , disabled, required, alt, help} = this.props;
        let rand = Math.round(Math.random()*100000);
        let {divError} = this.state;
        let helpDiv = '';
        let requiredDiv = '';

        if(required == "true")
        {
            requiredDiv = <sup className="required text-danger"> * </sup>;
        }
        if(help != undefined)
        {
            // helpDiv = <div className='alert alert-primary'> {help}</div>;
            helpDiv = <a href='#'  alt={help}><i class="fa fa-question-circle" aria-hidden="true"></i></a>;
        }

        if(type == "") type = "text";

        if(type=="hidden"){
            return (
                <input type={type}
                       defaultValue={defaultValue}
                       ref='item'
                />
            )
        }

        return(
            <div className={"form-group "+className}>
                <label>
                    {requiredDiv}
                    {label}
                    {helpDiv}
                </label>
                <div className="input-group curve">
                    <span className="input-group-addon">
                        <i className={icon}></i>
                    </span>
                    <input
                        type={type}
                        className="form-control"
                        placeholder={placeholder}
                        defaultValue = {defaultValue}
                        disabled={disabled}
                        ref='item'
                        required = {required}
                        alt = {alt}
                        key={'input'+rand}
                        {...options}
                    />
                </div>
                {divError}
            </div>
        );
    }

    renderSimple(){
        let {label, defaultValue, className, type,placeholder, required, help, options , disabled} = this.props;
        let rand = Math.round(Math.random()*100000);

        let {divError} = this.state;
        let helpDiv = '';
        let requiredDiv = '';

        if(required == "true")
        {
            requiredDiv = <sup className="required text-danger"> * </sup>;
        }
        if(help != undefined)
        {
            // helpDiv = <div className='alert alert-primary'> {help}</div>;
            helpDiv = <a href='#'  alt={help}><i class="fa fa-question-circle" aria-hidden="true"></i></a>;
        }

        if(type == "") type = "text";

        if(type=="hidden"){
            return (
                <input type={type}
                       defaultValue={defaultValue}
                       ref='item'
                />
            )
        }

        return(
            <div className={"form-group curve "+className}>
                <label>
                    {requiredDiv}
                    {label}
                    {helpDiv}
                </label>
                <input
                       type={type}
                       className="form-control"
                       placeholder={placeholder}
                       defaultValue = {defaultValue}
                       disabled={disabled}
                       ref='item'
                       key={'input'+rand}
                       {...options} />
                        {divError}
            </div>
        );
    }
}
