import React, { Component } from 'react';
import { FilePond as FilePondBase, File, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';

import FilePondPluginImageExifOrientation from 'filepond-plugin-image-exif-orientation';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import FilePondPluginImageCrop from 'filepond-plugin-image-crop';
import FilePondPluginImageEdit from 'filepond-plugin-image-edit';
import FilePondPluginImageTransform from 'filepond-plugin-image-transform';
import FilePondPluginImageResize from 'filepond-plugin-image-resize';

import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
import 'filepond-plugin-image-edit/dist/filepond-plugin-image-edit.css';

// FilePondPluginImageExifOrientation 
registerPlugin(
    FilePondPluginImageExifOrientation, 
    FilePondPluginImagePreview, 
    FilePondPluginImageCrop,
    FilePondPluginImageTransform,
    FilePondPluginImageResize,
    FilePondPluginImageEdit);
// FilePond.registerPlugin(
//     FilePondPluginFileValidateType,
//     FilePondPluginImageExifOrientation,
//     FilePondPluginImagePreview,
//     FilePondPluginImageCrop,
//     FilePondPluginImageResize,
//     FilePondPluginImageTransform,
//     FilePondPluginImageEdit
//   );

class Filepond extends Component{
    constructor(props) {
        super(props);

        this.state = {
            // Set initial files
            // files: ['index.html']
            files: []
        };
    }

    handleInit() {
        console.log('FilePond instance has initialised', this.pond);
    }

    render() {
        let {url} = this.props;
        return (
            <div className="form-group col-md-12">
            
                {/* Pass FilePond properties as attributes */}                
                <FilePondBase ref={ref => this.pond = ref}
                          allowMultiple={true}
                          labelIdle= "Drag & Drop your picture"
                          imagePreviewHeight= "170"
                          imageCropAspectRatio= '1:1'
                          imageResizeTargetWidth= "200"
                          imageResizeTargetHeight= "200"
                        //   imageEditEditor= {window.Doka.create()}
                          maxFiles={1} 
                          server={url}
                          oninit={() => this.handleInit() }
                          onupdatefiles={(fileItems) => {
                              // Set current file objects to this.state
                              this.setState({
                                  files: fileItems.map(fileItem => fileItem.file)
                              });
                          }}>
                    
                    {/* Update current files  */}
                    {this.state.files.map(file => (
                        <File key={file} src={file} origin="local" />
                    ))}
                    
                </FilePondBase>
                
            </div>
        );
    }
}

export {Filepond};