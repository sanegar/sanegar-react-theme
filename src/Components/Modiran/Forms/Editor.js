import React, {Component} from 'react';
import {Element} from './Element';

export class Editor extends Element {

    constructor(props){
        super(props);
        let rand = Math.ceil(Math.random() * 10000);
        let id = "editor"+rand;
        this.state = {id:id};
        // this.componentDidMount = this.componentDidMount.bind(this);
    }

    componentDidMount()
    {
        var config = {};

        // config.language = 'en';
        // this.createEnglishEditor(config);

        config.language = 'fa';
        config.font_names =
            "Tahoma;" +
            "Nazanin/Nazanin, B Nazanin, BNazanin;" +
            "Yekan/Yekan, BYekan, B Yekan, Web Yekan;" +
            "IranSans/IranSans, IranSansWeb;" +
            "Parastoo/Parastoo;" +
            "Arial/Arial, Helvetica, sans-serif;" +
            "Times New Roman/Times New Roman, Times, serif;";
        this.createPersianEditor(config);

        // this.createInlineEditor(config);

    }
    createPersianEditor(config) {
        var editor;
        editor = window.CKEDITOR.appendTo(this.state.id, config);
        editor.setData(this.props.defaultValue);
    }
    createEnglishEditor(config) {
        var editor;
        editor = window.CKEDITOR.appendTo(this.state.id, config);
        editor.setData(this.props.defaultValue);
    }
    createInlineEditor(config) {
        window.CKEDITOR.disableAutoInline = true;
        window.CKEDITOR.inline(this.state.id, config);
    }

    // componentWillReceiveProps(props){
    //     let val = props.defaultValue;
    //     if(val != undefined && val != "" && val != null)
    //     {
    //         this.refs.item.editor.setData(val);
    //     }
    // }

    render(){
        let {label, name, defaultValue, error , onChange ,className, required, options} = this.props;
        let {id,rand} = this.state;

        let divError = [];
        let requiredDiv = '';

        if(required == "true")
        {
            requiredDiv = <sup className="required text-danger"> * </sup>;
        }
        if(error!=undefined){
            error.map((err, index)=>{
                divError.push(<div className='alert alert-danger' id={'error-'+rand} key={index} style={{display:"block"}}>{err}</div>);
            })
        }

        return(
            <div className={"card mb-4 editor-card "+className}>
                 <label className="form-group has-float-label">
                    <div className="card-body">
                        {/* <textarea data-type='editor' id={id} ref='item' defaultValue={defaultValue} {...options}></textarea> */}

                        <textarea id={"content"+this.state.id} className="hide">
                            <p dir="rtl">
                                <span style="font-family:Tahoma">هم اکنون نوشتن اولین نوشته خود را آغاز کنید...</span>
                            </p>
                        </textarea>
                        <div id={this.state.id}></div>
                    </div>
                    <span>
                        {requiredDiv}
                        {label}
                    </span>
                </label>
                {divError}

            </div>

        );

    }
}
