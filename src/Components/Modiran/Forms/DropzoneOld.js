import React, { Component } from 'react';
import DropzoneComponent from 'react-dropzone-component';
import Config from '../../Config';
import axios from 'axios';
import {Element} from './Element';

class Dropzones extends Element{
    constructor(props) {
        super(props);
        this.state = {
          files: [],
        };
    }

    render() {
        // let {label, name, className, icon, url, deleteUrl, defaultValue, uploadDir} = this.props;
        let {label, name, className, icon, url, deleteUrl, defaultValue, uploadDir, maxFiles, maxFilesize, error,
            required, acceptType,maxImageWidth, maxImageHeight, minImageWidth, minImageHeight} = this.props;
        if(acceptType == undefined || acceptType == null || acceptType =="")
            acceptType = "image/*,application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document,video/*";
        if(maxFiles == undefined || maxFiles == null || maxFiles == "")
            maxFiles = 1;
        if(maxFilesize == undefined || maxFilesize == null || maxFilesize == "")
            maxFilesize = 1000;
        var vals = "";
        if(defaultValue != undefined && defaultValue != ""){
            if(typeof defaultValue != "string"){
                if(defaultValue.url != undefined){
                    vals = defaultValue.join("###");
                    if(vals != "") vals = vals + "###";
                }
                else{
                    defaultValue.forEach((item)=>{
                        if(item.url != undefined)
                            // vals += item.url+"###";
                            vals += item.url+"."+item.extension+"###";
                        else
                            vals += item+"###";
                            // vals += item.url+"."+item.extension+"###";
                    });
                }
            }
            // console.log(vals);
            var initDropzone = (myDropzone)=>{
                // console.log(defaultValue);
                this.state.dropzone = myDropzone;
                // console.log("def "+defaultValue);
                defaultValue.forEach(element => {
                    // console.log("element:");
                    // console.log(element);
                    let name = "";
                    // if(element.url != undefined) name = element.url;
                    if(element.url != undefined) name = element.url+"."+element.extension;
                    else name = element;
                    var mockFile = {name: name, size: element.size};
                    myDropzone.options.addedfile.call(myDropzone, mockFile);
                    myDropzone.options.thumbnail.call(myDropzone, mockFile, uploadDir+name);
                });
            }
        }
        // let rand = window.Math.random(1000, 9999);
        var componentConfig = { postUrl: Config.getHost()+url };
        let divError = [];
        if(error!=undefined){
            error.map((err, index)=>{
                divError.push(<div className='alert alert-danger' id={'error-'+rand} key={index} style={{display:"block"}}>{err}</div>);
            })
        }
        // let rand = window.Math.random(1000, 9999);
        // alert(123);
        var componentConfig = {
            postUrl: Config.getHost()+url,
            maxFiles: maxFiles,
        };
        var langs = {
            dictDefaultMessage: "رها کردن فایل جهت آپلود",
            dictFallbackMessage: "مرورگر شما، عمل کشیدن و رها کردن عکس را پشتیبانی نمی کند!!",
            dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
            dictFileTooBig: "حجم فایل از حدمجاز بیشتر می باشد ({{filesize}}MiB). حداکثر حجم فایل: {{maxFilesize}}MiB.",
            dictInvalidFileType: "آپلود فایل از این نوع مجاز نمی باشد.",
            dictResponseError: "سرور پاسخی با کد {{statusCode}} ارسال کرده است.",
            dictCancelUpload: "لغو آپلود",
            dictCancelUploadConfirmation: "آیا از کنسل شدن آپلود مطمئن هستید?",
            dictRemoveFile: "حذف فایل",
            dictMaxFilesExceeded: "شما نمی توانید فایل های بیشتری آپلود نمایید.",
        };
        var djsConfig = {
            autoProcessQueue: true,
            maxFiles: maxFiles,
            maxFilesize: maxFilesize,
            addRemoveLinks: true,
            acceptedFiles: acceptType,
            ...langs,
            init: function () {
                this.on("success", function(file, responseText) {
                    file.previewTemplate.setAttribute('id',responseText[0].id);
                });
                this.on("thumbnail", function(file) {
                    if (maxImageWidth != undefined && (file.width > maxImageWidth || file.height > maxImageHeight)) {
                        file.rejectMaxDimensions()
                    }
                    else if (minImageWidth != undefined && (file.width < minImageWidth || file.height < minImageHeight)) {
                        file.rejectMinDimensions()
                    }
                    else {
                        file.acceptDimensions();
                    }
                });
            },
            accept: function(file, done) {
                file.acceptDimensions = done;
                file.rejectMinDimensions = function() { done("طول و عرض تصویر حداقل باید  "+ minImageWidth+"×"+minImageHeight+" باشد!!"); };
                file.rejectMaxDimensions = function() { done("طول و عرض تصویر حداکثر باید "+ maxImageWidth+"×"+maxImageHeight+" باشد!!"); };
            }
        }
        var eventHandlers = {
            success:(file)=>{
                this.refs.item.value += file.xhr.response+"###";
                // console.log(file.xhr.response);
            },
            init: initDropzone,
            removedfile:(file)=>{
                // alert(file);
                let path = "";
                // console.log(file);
                if(file.xhr != null){
                    path = file.xhr.response;
                }
                else{
                    path = file.name;
                }
                axios.get(Config.getHost()+deleteUrl+path);
                this.refs.item.value = this.refs.item.value.replace(path+"###", "", "g");
            }
        }
        let rand = Math.random() * 1000;
        let requiredDiv = '';
        if(required == "true")
        {
            requiredDiv = <sup className="required text-danger"> * </sup>;
        }
        return (
          <div className={"form-group dropzoneDiv " + className}>
            <label>
                {label}
                {requiredDiv}
            </label>
                <div className="input-group">
                    <span className="input-group-addon">
                        <i className={icon}></i>
                    </span>
                    <DropzoneComponent config={componentConfig}
                        eventHandlers={eventHandlers}
                        djsConfig={djsConfig} key = {rand} />
                    <input type="hidden" ref='item' defaultValue = {vals}/>
                    {divError}
                </div>
          </div>

        );
    }

}





export {Dropzones};

