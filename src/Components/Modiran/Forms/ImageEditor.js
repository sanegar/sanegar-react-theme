import React, {Component} from 'react';
import {Element} from './Element';

import ImageEditorRc from 'react-cropper-image-editor';
import 'cropperjs/dist/cropper.css'; // see installation section above for versions of NPM older than 3.0.0
// If you choose not to use import, you need to assign Cropper to default
var cropper = require('react-cropper-image-editor').default

class ImageEditor extends Element {

  constructor(props){
    super(props);

    this.state = {image:{toDataURL: ()=>{} }};
  }

  saveData(){
    // console.log(data);
    const imageData = cropper.getImageData();
    // console.log(imageData);
  }

  render() {

    // let image = "https://www.lydaweb.com/storage/upload/images/December_2018/2542/total.png";
    // const saveData = this.saveData;
    const saveData = function(){alert('aa')};
    let image = "http://127.0.0.1:313/media/users/avatar/my%20pic.jpg";
    return (
        <div className="form-group col-md-12">
            <ImageEditorRc
                ref='cropper'
                crossOrigin='true' // boolean, set it to true if your image is cors protected or it is hosted on cloud like aws s3 image server
                src={image}
                style={{height: 300, width: 400}}
                aspectRatio={16 / 9}
                className={'image-editor'}
                guides={true}
                rotatable={true}
                aspectRatio={16 / 16}
                imageName='image name with extension to download'
                saveImage={saveData} // it has to catch the returned data and do it whatever you want
                responseType='blob/base64'
                guides={false}/>
        </div>
    );
  }
}

export {ImageEditor};
