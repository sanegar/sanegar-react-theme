import React, {Component} from 'react';
import {Element} from './Element';


class Textarea extends Element {
    constructor(props){
        super(props);
        this.state = {divError:[]};
        this.handleChange = this.handleChange.bind(this)
    }
    handleChange(event) {
        this.setState({value: event.target.value});
    }

    componentWillReceiveProps(props){
         this.setState({value: props.defaultValue});
    }

    render(){
        let {icon, required, error} = this.props;
        let rand = Math.random(1000, 9999);
        let divError = [];
        let requiredDiv = '';

        if(required == "true")
        {
            requiredDiv = <sup className="required text-danger"> * </sup>;
        }
        if(error!=undefined){
            error.map((err, index)=>{
                divError.push(<div className='alert alert-danger' id={'error-'+rand} key={index} style={{display:"block"}}>{err}</div>);
            })
        }
        this.state.divError = divError;

        if(icon == undefined)
            return this.renderSimple();
        else
            return this.renderWithIcon();
    }

    renderWithIcon(){
        let {label, defaultValue, icon, className, row, alt, required} = this.props;
        let {divError, value} = this.state;
        let requiredDiv = '';

        if(required == "true")
        {
            requiredDiv = <sup className="required text-danger"> * </sup>;
        }
        if (row==undefined) row = 5;

        return(
            <div className={"form-group "+className}>
                <label>
                    {requiredDiv}
                    {label}
                </label>
                <div className="input-group">
                    <span className="input-group-addon">
                        <i className={icon}></i>
                    </span>
                    <textarea
                        className="form-control"
                        rows={row}
                        placeholder={label}
                        defaultValue = {value}
                        onChange={this.handleChange}
                        alt = {alt}
                        required = {required}
                        ref='item'></textarea>
                </div>
                {divError}
            </div>
        );
    }

    renderSimple(){
        let {label, defaultValue, className, required, row} = this.props;
        let {divError, value} = this.state;
        let requiredDiv = '';

        if(required == "true")
        {
            requiredDiv = <sup className="required text-danger"> * </sup>;
        }
        if (row==undefined) row = 5;

        return(
            <div className={"form-group curve "+className}>
                <label>
                    {requiredDiv}
                    {label}
                </label>
                <textarea
                    className="form-control"
                    rows={row}
                    placeholder={label}
                    defaultValue = {value}
                    onChange={this.handleChange}
                    ref='item'></textarea>
                {divError}
            </div>
        );
    }
}

export {Textarea};
