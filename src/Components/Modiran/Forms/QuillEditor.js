import React, {Component} from 'react';
import {Element} from './Element';


export class QuillEditor extends Element {
    constructor(props){
        super(props);
        let rand = Math.ceil(Math.random() * 10000);
        let id = "editor"+rand;
        this.state = {id:id};
        this.componentDidMount = this.componentDidMount.bind(this);
    }
    componentDidMount()
    {
        let {defaultValue} = this.props;
        let {id} = this.state;
        window.ClassicEditor.create(document.querySelector("#"+id))
                .then((editor)=>{
                    this.refs.item.editor = editor;
                    let val = defaultValue;
                    if(val != undefined && val != "" && val != null && val != "undefined")
                    {
                        editor.setData( val );
                    }


                })
                .catch(e => {});
    }

    componentWillReceiveProps(props){
        let val = props.defaultValue;
        if(val != undefined && val != "" && val != null)
        {
            this.refs.item.editor.setData(val);
        }
    }

    render(){
        let {label, name, defaultValue, error , onChange ,className , required, options} = this.props;
        let {id,rand} = this.state;

        let divError = [];
        let requiredDiv = '';

        if(required == "true")
        {
            requiredDiv = <sup className="required text-danger"> * </sup>;
        }

        if(error!=undefined){
            error.map((err, index)=>{
                divError.push(<div className='alert alert-danger' id={'error-'+rand} key={index} style={{display:"block"}}>{err}</div>);
            })
        }

        return(
            <div className={"card mb-4 editor-card "+className}>
                <label className="form-group has-float-label">
                    <div className="card-body">
                        <textarea data-type='editor' id={id} ref='item' defaultValue={defaultValue} {...options}></textarea>
                    </div>
                    <span>
                        {requiredDiv}
                        {label}
                    </span>
                </label>
                {divError}
            </div>
        );

    }
}
