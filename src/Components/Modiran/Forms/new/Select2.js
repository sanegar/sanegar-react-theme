import React from 'react';
import {Element} from './Element';
// import $ from 'jquery';
// import select2 from 'select2';

class Select2 extends Element{
    constructor(props){
        super(props);
        this.state = {id:"", divError:""};
        this.prepare = this.prepare.bind(this);
    }

    prepare(){
        let {defaultValue, error, placeholder, options, data} = this.props;
        let id = "select"+(new Date()).getTime() + Math.round()*1000;
        this.state.id = id;
        if( typeof defaultValue != "number" && typeof defaultValue != "string" && defaultValue != undefined && defaultValue.length > 0){
            let temp = [];
            defaultValue.map((item)=>{
                temp.push(item.id);
            });
            defaultValue = temp;
        }
        window.$(document).ready(function() {
            window.$(`#${id}`).select2({
                theme: "bootstrap",
                placeholder: placeholder,
                data: data,
                maximumSelectionSize: 6
            }).on("select2:select",(e)=>{
                if (options!=undefined) {
                    options.onChange(e)
                }
            });
            window.$(`#${id}`).val(defaultValue).trigger("change");
        });

        this.state.divError = this.checkError();
    }

    render(){
        let {label, multiple, className, options} = this.props;
        this.prepare();
        let {id, divError} = this.state;

        return(
            <div className={className}>
                <label className={"form-group has-float-label"}>
                    <select className="form-control select2-single"
                        tabIndex="-1"
                        aria-hidden="true"
                        id = {id}
                        ref="item"
                        multiple={multiple}
                        data ={(options!=undefined)?options.data:""}
                    >
                        {
                            this.props.children
                        }
                    </select>

                    {/* <input class="select2" id = {id} placeholder="select one"/> */}

                    <span>{label}</span>
                    {divError}
                </label>
            </div>
        );
    }
}

export {Select2};
