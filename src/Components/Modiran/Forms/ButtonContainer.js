import React from 'react';

export const ButtonContainer = (props) => {
    let {className, children} = props;
    // if (className == undefined) className = 'btn-info';
    // if(icon == undefined) icon = "icon-check";

    return(        
        <div className={"form-actions " + className}>
            {children}
        </div>        
    );
}