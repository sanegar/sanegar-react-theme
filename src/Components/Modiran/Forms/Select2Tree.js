import React, {Component} from 'react';
import {Select2} from './Select2';

class Select2Tree extends Select2{
    constructor(props){
        super(props);
    }

    prepare(){
        let {placeholder, defaultValue} = this.props;
        // alert(123);
        let id = "select"+(new Date()).getTime() + Math.round()*1000;
        this.state.id = id;
        let {data} = this.props;
        if( typeof defaultValue != "number" && typeof defaultValue != "string" && defaultValue != undefined && defaultValue.length > 0){
            let temp = [];
            defaultValue.map((item)=>{
                temp.push(item.id);
            });
            defaultValue = temp;
        }
        // console.log("Select2Tree: ");
        // console.log(defaultValue);
        $(document).ready(function() {
            $("#"+id).select2ToTree({treeData: {dataArr: data}, maximumSelectionLength: 3, theme: "bootstrap",placeholder: placeholder});
            $(`#${id}`).val(defaultValue).trigger("change");
        });
    }

    render(){
        let {label, multiple, className, options} = this.props;
        this.prepare();
        let {id, divError} = this.state;

        return(
            <div className={className}>
                <label className={"form-group has-float-label"}>
                    <select className="form-control select2-single"
                        tabIndex="-1" aria-hidden="true"
                        id = {id}
                        ref="item"
                        multiple={multiple}
                        data ={(options!=undefined)?options.data:""}
                    >
                        {/* {
                            this.props.children
                        } */}
                    </select>

                    {/* <input class="select2" id = {id} placeholder="select one"/> */}

                    <span>{label}</span>
                    {divError}
                </label>
            </div>
        );
    }
}

export {Select2Tree};
