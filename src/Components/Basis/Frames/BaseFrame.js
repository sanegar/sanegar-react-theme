import React, {Component} from 'react';

class BaseFrame extends Component{
    constructor(props){
        super(props);
    }

    render(){
        let {label,children,options,leftSide,className} = this.props;
        if(className == undefined) className = "col-md-12";
        return(
            <div className={"card mb-4 "+className} id="contentFrame">
                <div className="card-body">
                    {(options == undefined)?<h5 className="card-title mb-4"> {label} {leftSide} </h5>:<h5 className="card-title mb-4"> {label} {leftSide} <i {...options}></i></h5>}
                    {
                        children
                    }
                </div>
            </div>
        );
    }
}

export {BaseFrame};
