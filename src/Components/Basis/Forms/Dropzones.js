import React from 'react';
import {Element} from './Element';
import DropzoneComponent from 'react-dropzone-component';
import Config from '../../Config';
import axios from 'axios';

class Dropzones extends Element{
    constructor(props) {
        super(props);

        this.state = {
          files: [],
          dropzone: null
        };
    }


    render() {
        let {label, name, className, icon, url, deleteUrl, defaultValue,
            uploadDir, maxFiles, maxFileSize, error , acceptType,required,help,
            maxImageWidth, maxImageHeight, minImageWidth, minImageHeight} = this.props;
        if(uploadDir == undefined) uploadDir = "";
        if(uploadDir.substr(-1, 1) != "/") uploadDir += "/";

        if(acceptType == undefined || acceptType == null || acceptType =="")
        acceptType = "image/*,application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document,video/*";

        if(maxFiles == undefined || maxFiles == null || maxFiles == "")
        maxFiles = 1;

        if(maxFileSize == undefined || maxFileSize == null || maxFileSize == "")
        maxFileSize = 1000;

        var vals = "";
        // console.log("defaultValue");
        // console.log(defaultValue);
        if(defaultValue != undefined && defaultValue != ""){
            if(typeof defaultValue != "string"){
                if(defaultValue.url != undefined){
                    vals = defaultValue.join("###");
                    if(vals != "") vals = vals + "###";
                }
                else{
                    defaultValue.forEach((item)=>{
                        if(item.url != undefined){
                            if(item.extension != undefined)
                                vals += item.url+"."+item.extension+"###";
                            else
                                vals += item.url+"###";
                        }
                        else
                            vals += item+"###";
                    });
                }
            }
            else{
                defaultValue = defaultValue.split('###');
                if(defaultValue[defaultValue.length - 1] == "")
                    delete defaultValue[defaultValue.length - 1];
                // defaultValue = [defaultValue];
            }
            var initDropzone = (myDropzone)=>{
                this.state.dropzone = myDropzone;
                // console.log(defaultValue);
                defaultValue.forEach(element => {
                    let name = "";
                    if(element.url != undefined) name = element.url+"."+element.extension;
                    else name = element;

                    var mockFile = {name: name, size: element.size};
                    let filePath = uploadDir+name;
                    // console.log("mockFile");
                    // console.log(mockFile);
                    // console.log("filePath");
                    // console.log(filePath);
                    if(filePath.substr(-1, 2) != "//") filePath = filePath.replace('//', '/');
                    myDropzone.options.addedfile.call(myDropzone, mockFile);
                    myDropzone.options.thumbnail.call(myDropzone, mockFile, filePath);
                });
            }
        }

        let divError = this.checkError();

        var componentConfig = {
            postUrl: Config.getHost()+url,
            maxFiles: maxFiles,

        };

        var langs = {
            dictDefaultMessage: "رها کردن فایل جهت آپلود",
            dictFallbackMessage: "مرورگر شما، عمل کشیدن و رها کردن عکس را پشتیبانی نمی کند!!",
            dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
            dictFileTooBig: "حجم فایل از حدمجاز بیشتر می باشد ({{filesize}}MiB). حداکثر حجم فایل: {{maxFilesize}}MiB.",
            dictInvalidFileType: "آپلود فایل از این نوع مجاز نمی باشد.",
            dictResponseError: "سرور پاسخی با کد {{statusCode}} ارسال کرده است.",
            dictCancelUpload: "لغو آپلود",
            dictCancelUploadConfirmation: "آیا از کنسل شدن آپلود مطمئن هستید?",
            dictRemoveFile: "حذف فایل",
            dictMaxFilesExceeded: "شما نمی توانید فایل های بیشتری آپلود نمایید.",
        };

        // var maxImageWidth = 500, maxImageHeight = 500;

        var djsConfig = {
            autoProcessQueue: true,
            maxFiles: maxFiles,
            maxFilesize: maxFileSize,
            addRemoveLinks: true,
            acceptedFiles: acceptType,
            ...langs,
            init: function () {
                this.on("success", function(file, responseText) {
                    file.previewTemplate.setAttribute('id',responseText[0].id);
                });
                this.on("thumbnail", function(file) {
                    if (maxImageWidth != undefined && (file.width > maxImageWidth || file.height > maxImageHeight)) {
                        file.rejectMaxDimensions()
                    }
                    else if (minImageWidth != undefined && (file.width < minImageWidth || file.height < minImageHeight)) {
                        file.rejectMinDimensions()
                    }
                    else {
                        file.acceptDimensions();
                    }
                });
            },
            accept: function(file, done) {
                file.acceptDimensions = done;
                file.rejectMinDimensions = function() { done("طول و عرض تصویر حداقل باید  "+ minImageWidth+"×"+minImageHeight+" باشد!!"); };
                file.rejectMaxDimensions = function() { done("طول و عرض تصویر حداکثر باید "+ maxImageWidth+"×"+maxImageHeight+" باشد!!"); };
            }
        }

        var eventHandlers = {
            success:(file)=>{
                this.refs.item.value += file.xhr.response+"###";
                // console.log(file.xhr.response);
            },
            init: initDropzone,
            removedfile:(file)=>{
                // alert(file);
                let path = "";
                // console.log(file);
                if(file.xhr != null){
                    path = file.xhr.response;
                }
                else{
                    path = file.name;
                }

                // console.log(deleteUrl);
                let deletePath = path.split('/');
                // console.log(path);
                // console.log(deletePath[deletePath.length-1]);
                axios.get(Config.getHost()+deleteUrl+deletePath[deletePath.length-1]);
                this.refs.item.value = this.refs.item.value.replace(path+"###", "", "g");
            }
        }

        // console.log('vals');
        // console.log(vals);

        let rand = Math.random() * 1000;

        let helpDiv = '';
        let requiredDiv = '';


        if(required == "true")
        {
            requiredDiv = <sup className="required text-danger"> * </sup>;
        }
        if(help != undefined)
        {
            // helpDiv = <div className='alert alert-primary'> {help}</div>;
            helpDiv = <a href='#'  alt={help}><i class="fa fa-question-circle" aria-hidden="true"></i></a>;
        }

        return (
            <div className={"form-group  dropzone-wrapper " + className}>
                <label className={"form-group has-float-label "}>
                    <div className="form-group">
                        <DropzoneComponent config={componentConfig}
                            eventHandlers={eventHandlers}
                            djsConfig={djsConfig} key = {rand}/>
                        <input type="hidden" ref='item' defaultValue = {vals} />
                    </div>
                    <span>
                        {requiredDiv}
                        {label}
                        {helpDiv}
                    </span>
                </label>
                {divError}
            </div>
          );
    }
}


export {Dropzones};
