import React from 'react';
import {Element} from './Element';

export class CheckBox extends Element {
    constructor(props){
        super(props);
    }

    render(){
        let {label, name, value, defaultValue, className ,error,onChange} = this.props;
        let rand = Math.random(1000, 9999);
        // console.log(defaultValue);
        let divError = this.checkError();
        return(
            <div className="form-group row" >
                <label className="col-2 col-form-label">{label}</label>
                <div className="col-10">
                    <div className="custom-switch custom-switch-secondary mb-2">
                        <input onChange={onChange} name={name} key={rand} ref='item' className="custom-switch-input"
                            id={"switch2"+rand} type="checkbox" defaultChecked={defaultValue==1?true:false} value={1} />
                        <label className="custom-switch-btn" htmlFor={"switch2"+rand}  dir="ltr"></label>
                        {divError}
                    </div>
                </div>
            </div>
        );
    }
}
