import React from 'react';
import Lang from '../Tools/Lang';

export const Button = (props) => {
    let {label, onClick, className} = props;

    className == undefined ?className="btn btn-primary":className=className;
    return(
        <button className={className} type="button" onClick={(event)=>{
                        window.event = event;
                        onClick(event)}
                    }>
            {Lang('public.'+label)}
            <span className="loading"></span>
        </button>
    );
}
