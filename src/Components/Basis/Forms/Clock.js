import React, {Component} from 'react';

export class Clock extends Component {

    componentDidMount()
    {
        window.$(".clockpicker").clockpicker({
            "donetext": "قبول"
        });
    }

    render() {
        let {label, name, className ,error,defaultValue} = this.props;
        let rand = Math.round(Math.random()*100000);

        return (
            <div className="portlet-body">
                <div className="form-group">
                    {/* <label>ساعت رزرو میز</label> */}
                    <div className="input-group">
                        <span className="input-group-addon">
                            <i className="icon-clock"></i>
                        </span>
                        <input name={name} ref='item' key={'input'+rand} defaultValue = {defaultValue} type="text" className="form-control clockpicker" />
                    </div>
                </div>
            </div>
        );
    }
}
