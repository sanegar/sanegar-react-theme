import React from 'react';
import {Element} from './Element';
import Lang from '../Tools/Lang';

class Textarea extends Element {
    constructor(props){
        super(props);
        this.state = {value:""};
        // this.handleChange = this.handleChange.bind(this);
    }

    render(){
        let {label, defaultValue, className, row, options, required, help, placeholder, disabled} = this.props;
        let divError = this.checkError();
        let requiredDiv = '';
        let helpDiv = '';

        
            //__________ labels translate ______________
                let labels = label.split('-');
                let labelTranslate='';
                if((labels.length)>0)
                {
                    labels.forEach((slice)=>{
                        labelTranslate += Lang('public.'+slice);
                        labelTranslate += " ";
                    });
                }
            //__________ placeholder translate ______________
                let placeholderS = placeholder.split('-');
                    let placeholderTranslate='';
                    if((placeholderS.length)>0)
                    {
                        placeholderS.forEach((slice)=>{
                            placeholderTranslate += Lang('public.'+slice);
                            placeholderTranslate += " ";
                        });
                    }


        if(required == "true")
        {
            requiredDiv = <sup className="required text-danger"> * </sup>;
        }
        if(help != undefined)
        {
            helpDiv = <a href='#'  alt={help}><i class="fa fa-question-circle" aria-hidden="true"></i></a>;
        }
        if (row==undefined) row = 5;

        return(
            <div className={"form-group curve "+className}>
                <label className="form-group has-float-label">
                    <textarea disabled={(disabled == undefined)?false:true} className="form-control" key={Math.random()*1000}
                        style={{paddingTop: '10px'}}
                        rows={row} 
                        placeholder={Lang('public.'+placeholder)}
                        defaultValue = {defaultValue} ref='item'
                        onFocus = {()=>this.removeError()}
                        {...options}></textarea>
                    <span>
                        {requiredDiv}
                        {Lang('public.'+label)}
                        {helpDiv}
                    </span>
                </label>
                {divError}
            </div>
        );
    }
}

export {Textarea};
