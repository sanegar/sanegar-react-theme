import React, {Component} from 'react';
import {Element} from './Element';

export class RadioBtn extends Element {
    constructor(props){
        super(props);

        this.state = {checkDefaultValue: true};
        this.setDefaultValue = this.setDefaultValue.bind(this);
    }

    setDefaultValue(){
        let {name, defaultValue} = this.props;
        let radios = document.getElementsByName(name);
        radios.forEach((item)=>{
            if (item.value == defaultValue){
                document.getElementById(item.id).checked = 'checked';
            }
        });
    }

    componentDidMount(){
        this.setDefaultValue();
    }

    componentWillReceiveProps(props){
        this.setDefaultValue();
    }

    render(){

        let {label, name, data, defaultValue, classNameDiv, classLabel, classIcon, onChange, required, help} = this.props;
        let {checkDefaultValue} = this.state;
        // if(onChange == undefined) onChange =
        let rand = Math.random(1000, 9999);
        let divError = [];
        let requiredDiv = '';
        let helpDiv = '';
        if(required == "true")
        {
            requiredDiv = <sup className="required text-danger"> * </sup>;
        }
        if(help != undefined)
        {
            helpDiv = <a href='#'  alt={help}><i class="fa fa-question-circle" aria-hidden="true"></i></a>;
        }
        // if(error!=undefined){
        //     error.map((err, index)=>{
        //         divError.push(<div className='alert alert-danger' id={'error-'+rand} key={index} style={{display:"block"}}>{err}</div>);
        //     })
        // }

        // console.log(name +' defaultValue:');
        // console.log(defaultValue);

        return(<div className={classNameDiv}>
            {/* <h6> <i className="glyph-icon fas fa-angle-left"></i>{label}</h6> */}
            <h6>
                <i className={"glyph-icon "+ classIcon}></i>
                {requiredDiv}
                {label}
                {helpDiv}
            </h6>
            <div className="mb-4 pr-5">
                {
                    data.map((item, index)=>
                    <div className={"custom-control custom-radio "+item.className} key={index}>
                        {
                            <input  type="radio"
                                key={index}
                                ref={'item'+index}
                                value={item.value}
                                id={name+index}
                                name={name}
                                className={"custom-control-input "}
                                onChange={onChange}
                            />
                        }
                        <label className={"custom-control-label "} htmlFor={name+index}>{item.label}
                        </label>
                    </div>
                    )
                }

            </div>
        </div>

        );
    }
}
