import React from 'react';

export const ButtonContainer = (props) => {
    let {className, children} = props;

    return(
        <div className={"form-actions " + className}>
            {children}
        </div>
    );
}
