import React from 'react';
import {Element} from './Element';
import Lang from '../Tools/Lang';

class Select2 extends Element{
    constructor(props){
        super(props);
        this.state = {id:"", divError:""};

        this.createId = this.createId.bind(this);
        this.createSelect2 = this.createSelect2.bind(this);
    }

    createId(){
        let id = "select"+(new Date()).getTime()+Math.ceil(Math.random()*10000);
        this.state.id = id;
    }

    createSelect2(){
        let {defaultValue, placeholder, options, data} = this.props;
        let id = this.state.id;

        if( typeof defaultValue != "number" && typeof defaultValue != "string" && defaultValue != undefined && defaultValue.length > 0){
            let temp = [];
            defaultValue.map((item)=>{
                temp.push(item.id);
            });
            defaultValue = temp;
        }

        window.$(document).ready(function() {
            window.$(`#${id}`).select2({
                width: '100%',
                theme: "bootstrap",
                data: data,
                // rtl: true,
                allowClear: true,
                placeholder:Lang('public.'+placeholder),
                maximumSelectionSize: 6
            }).on("select2:select",(e)=>{
                if (options!=undefined) {
                    options.onChange(e)
                }
            }).on('focus', function (evt) {
                self.removeError();
            });
            window.$(`#${id}`).val(defaultValue).trigger("change");
        })
        // return id;
        this.state.divError = this.checkError();
    }

    prepare(){
        let {defaultValue, placeholder, options, data} = this.props;
        let id = "select"+(new Date()).getTime() + Math.ceil(Math.round()*10000);
        this.state.id = id;
        let self = this;
        if( typeof defaultValue != "number" && typeof defaultValue != "string" && defaultValue != undefined && defaultValue.length > 0){
            let temp = [];
            defaultValue.map((item)=>{
                temp.push(item.id);
            });
            defaultValue = temp;
        }
        window.$(document).ready(function() {
            window.$(`#${id}`).select2({
                theme: "bootstrap",
                placeholder:Lang('public.'+placeholder),
                data: data,
                allowClear: true,
                maximumSelectionSize: 6
            }).on("select2:select",(e)=>{
                if (options!=undefined) {
                    options.onChange(e)
                }
            }).on('focus', function (evt) {
                // alert(222);
                self.removeError();
              });
            window.$(`#${id}`).val(defaultValue).trigger("change");
        });

        this.state.divError = this.checkError();
    }

    render(){
        let {label, multiple, className, options, required, help, disabled} = this.props;
        this.createId();
        this.createSelect2();
        let {id, divError} = this.state;
        let helpDiv = '';
        let requiredDiv = '';

        if(required == "true")
        {
            requiredDiv = <sup className="required text-danger"> * </sup>;
        }
        if(help != undefined)
        {
            // helpDiv = <div className='alert alert-primary'> {help}</div>;
            helpDiv = <a href='#'  alt={help}><i class="fa fa-question-circle" aria-hidden="true"></i></a>;
        }

        return(
            <div className={className}>
                <label className={"form-group has-float-label"}>
                    <select
                        id = {id}
                        ref="item"
                        className="form-control select2-single"
                        tabIndex="-1"
                        aria-hidden="true"
                        multiple={multiple}
                        disabled={(disabled == undefined)?false:true}
                        data ={(options!=undefined)?options.data:""}
                    >
                        {
                            this.props.children
                        }
                    </select>

                    {/* <input class="select2" id = {id} placeholder="select one"/> */}

                    <span>
                        {requiredDiv}
                        {Lang('public.'+label)}
                        {helpDiv}
                    </span>
                    {divError}
                </label>
            </div>
        );
    }
}

export {Select2};
