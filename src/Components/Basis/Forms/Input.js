import React from 'react';
import {Element} from './Element';
import Lang from '../Tools/Lang';

export class Input extends Element {
    constructor(props){
        super(props);
    }

    render(){
        let {label, defaultValue, error, className, type, placeholder, options , disabled, inputClassName , id, required, help} = this.props;
        let divError = this.checkError();
        let {rand} = this.state;

        let helpDiv = '';
        let requiredDiv = '';

        if(required == "true")
        {
            requiredDiv = <sup className="required text-danger"> * </sup>;
        }
        if(help != undefined)
        {
            // helpDiv = <div className='alert alert-primary'> {help}</div>;
            helpDiv = <a href='#'  alt={help}><i class="fa fa-question-circle" aria-hidden="true"></i></a>;
        }

        if(type=="hidden"){
            return (
                <input type={type}
                       defaultValue={defaultValue}
                       ref='item'
                       id={id}
                />
            )
        }

        // //__________ labels translate ______________
        //     let labels = label.split('-');
        //     let labelTranslate='';
        //     if((labels.length)>0)
        //     {
        //         labels.forEach((slice)=>{
        //             labelTranslate += Lang('public.'+slice);
        //             labelTranslate += " ";
        //         });
        //     }
        // //__________ placeholder translate ______________
        //     let placeholderS = placeholder.split('-');
        //         let placeholderTranslate='';
        //         if((placeholderS.length)>0)
        //         {
        //             placeholderS.forEach((slice)=>{
        //                 placeholderTranslate += Lang('public.'+slice);
        //                 placeholderTranslate += " ";
        //             });
        //         }



        return(
            <div className = {className}>
                <label className={"form-group has-float-label "}>
                    <input className={"form-control "+inputClassName}
                           defaultValue = {defaultValue}
                           ref='item'
                           disabled={disabled}
                           placeholder={Lang('public.'+placeholder)}
                           onFocus={()=>this.removeError()}
                           key={'input'+rand}
                           {...options}
                           id={id}
                    />
                    <span>
                        {requiredDiv}
                        {Lang('public.'+label)}
                        {helpDiv}
                    </span>
                    {divError}
                </label>
            </div>
        );
    }
}
