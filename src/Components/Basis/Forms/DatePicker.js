import React from 'react';
import {Element} from './Element';

export class DatePicker extends Element {
    constructor(props){
        super(props);
    }

    componentDidMount(){

        setTimeout(()=>{
               if (this.props.options!=undefined) {
                    let {onChange,data} = this.props.options;
                    data = data.replace(".","-");
                    window.$("."+data+' .datepicker').persianDatepicker({
                        format: 'YYYY/MM/DD',
                        inline: false,
                        initialValue: false,
                        onSelect:()=>{
                            let target = $("."+data+' .datepicker')[0];
                            let e = {target}
                            onChange(e);
                        }
                    })
                }else{
                    window.$('.datepicker').persianDatepicker({
                        format: 'YYYY/MM/DD',
                        inline: false,
                        initialValue: false,
                    })
                }
            }
        , 1000);
    }

    render(){
        let {name, label, className, defaultValue, error, placeholder, options, required, help} = this.props;
        let divError = this.checkError();
        let helpDiv = '';
        let requiredDiv = '';

        if(required == "true")
        {
            requiredDiv = <sup className="required text-danger"> * </sup>;
        }
        if(help != undefined)
        {
            // helpDiv = <div className='alert alert-primary'> {help}</div>;
            helpDiv = <a href='#'  alt={help}><i class="fa fa-question-circle" aria-hidden="true"></i></a>;
        }


        return(
            <React.Fragment>
                <div className={className}>
                    <label className={"form-group has-float-label "}>
                        <input className="form-control datepicker"
                            defaultValue = {defaultValue}
                            // value = {defaultValue}
                            ref='item'
                            name={name}
                            placeholder = {placeholder}
                            data={(options!=undefined)?options.data:""}
                        />
                        <span>
                            {requiredDiv}
                            {label}
                            {helpDiv}
                        </span>
                        {divError}
                    </label>
                </div>
            </React.Fragment>
        );
    }
}
