import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import $ from 'jquery';
import {Lang} from './../';
// import {routes} from '../../routes1';

class BreadCrumb extends Component{
    constructor(props){
        super(props);
        this.state = {
            location: '',
            btnDisplay:"none",
            pathAssets: [],
        };
        this.handlePAth = this.handlePAth.bind(this);
    }

    componentWillMount() {
        this.handlePAth(this.props.location.pathname);
    }

    componentDidUpdate(prevProps, prevState) {
        this.handlePAth(this.props.location.pathname);
    }

    handlePAth(path){
        // For Aviod LOOP
        if(this.state.location == path)
          return;

        let {routes} = this.props;

        $('#newDIV').css('display', 'none');

        let items = path.split("/");
        // console.log(items.length);
        let n = items.length;
        let pItems = ["/"];
        let pages = [];
        let last = "";
        for(let i=1; i < n; i++){
            // console.log(123);
            if(parseInt(items[i]) > 0)
             items[i] = ":id";

            last += "/"+items[i];
            pItems.push(last);
        }

        routes.forEach((r)=>{
            pItems.forEach((page, index)=>{
                // if(r.path == page){
                if("/"+r.group+r.path == page){
                    pages.push({path: page, label: r.label, subLabel: r.subLable});
                    delete pItems[index];
                }
            })
        })

        this.setState({pathAssets: pages, location: path});
        // console.log(pages);
    }

    render(){
        const {pathAssets, btnDisplay} = this.state;
        let count = pathAssets.length;
        if(count == 0) return(<div></div>);
        return(
            <React.Fragment>
                {/* <h1 className="formTitle">{pathAssets[count-1].label} {Lang('public.'+pathAssets[count-1].label)}</h1> */}
                <h1 className="formTitle">{Lang('public.'+pathAssets[count-1].label)}</h1>
                <nav className="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                    <ol className="breadcrumb pt-0">
                        {
                            pathAssets.map((path, index)=>
                                <li key={"bread"+index} className={(index+1 == count)? "breadcrumb-item active":"breadcrumb-item"}>
                                    <Link key={"link"+index} to={path.path}> {Lang('public.'+path.subLabel)} </Link>
                                </li>
                            )
                        }
                    </ol>
                </nav>
                <div className="float-sm-left text-zero" id="newDIV" style={{display:btnDisplay}}>
                    <a style={{color:"#FFF"}} href="" id="newBTN" className="btn btn-primary btn-lg ml-1"> مورد جدید </a>
                </div>
            </React.Fragment>
        );
    }
}

// export {BreadCrumb};

export default connect((state)=>({routes: state.routes}))(BreadCrumb);

