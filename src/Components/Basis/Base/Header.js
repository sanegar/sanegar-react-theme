import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class Header extends Component{
    constructor(props){
        super(props);
    }

    render(){

        return(
        <nav className="navbar blueNav fixed-top">
            {
                /* <div className="search" data-search-path="Layouts.Search03d2.html?q=">
                    <input placeholder="جستجو..." />
                    <span className="search-icon">
                        <i className="flaticon-search4"></i>
                    </span>
                </div> */
            }

            <Link className="navbar-logo" to="/">
                <span className="d-none d-xs-block">
                    <img src="/images/logo0.png" className="headerLogo" />
                </span>
                <span className="logo-mobile d-block d-xs-none">
                    <img src="/images/logo0.png" className="headerLogo" />
                </span>
            </Link>

            <div className="ml-auto">
                <div className="user d-inline-block">
                    <button className="btn btn-empty p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span>
                            <img alt="Profile Picture" src={"/media/users/images/"+((window.MyInfo.pic!="")?window.MyInfo.pic:"man.png")} />
                        </span>
                        <span className="name">{window.MyInfo.name}</span>
                    </button>

                    <div className="dropdown-menu dropdown-menu-right mt-3">
                         <Link className="dropdown-item" to="/users/changeProfile">حساب کاربری</Link>
                        <Link className="dropdown-item" to="/users/changePass">تغییر رمز عبور</Link>
                        <Link className="dropdown-item" to="/exit">خروج</Link>
                        {/* <a className="dropdown-item" onClick={()=>{window.document.getElementById('logoutForm').submit();}} href="#">خروج</a> */}
                    </div>
                </div>

                <div className="header-icons d-inline-block align-middle">

                    {/* <div className="position-relative d-none d-sm-inline-block">
                        <button className="header-icon btn btn-empty" type="button" id="iconMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i className="flaticon-shapes-1"></i>
                        </button>
                        <div className="dropdown-menu dropdown-menu-right mt-3 position-absolute" id="iconMenuDropdown">
                            <a href="#" className="icon-menu-item">
                                <i className="flaticon-settings d-block"></i>
                                <span>تنطیمات</span>
                            </a>

                            <a href="#" className="icon-menu-item">
                                <i className="flaticon-user-1 d-block"></i>
                                <span>کاربران</span>
                            </a>

                            <a href="#" className="icon-menu-item">
                                <i className="flaticon-document d-block"></i>
                                <span>نظرسنجی</span>
                            </a>

                            <a href="#" className="icon-menu-item">
                                <i className="flaticon-suitcase d-block"></i>
                                <span>وظایف</span>
                            </a>

                        </div>
                    </div>

                    <div className="position-relative d-inline-block">
                        <button className="header-icon btn btn-empty" type="button" id="notificationButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i className="flaticon-music"></i>
                        </button>
                        <div className="dropdown-menu dropdown-menu-right mt-3 scroll position-absolute" id="notificationDropdown">

                            <div className="d-flex flex-row mb-3 pb-3 border-bottom">
                                <a href="#">
                                    <img src="assets/img/profile-pic-l-2.jpg" alt="Notification Image" className="img-thumbnail list-thumbnail xsmall border-0 rounded-circle" />
                                </a>
                                <div className="pr-3 pl-2">
                                    <a href="#">
                                        <p className="font-weight-medium mb-1">علی علیزاده فقط نظر جدیدی ارسال کرد!</p>
                                        <p className="text-muted mb-0 text-small"><span className="numberltr">09.04.2018 - 12:45</span></p>
                                    </a>
                                </div>
                            </div>

                            <div className="d-flex flex-row mb-3 pb-3 border-bottom">
                                <a href="#">
                                    <img src="assets/img/notification-thumb.jpg" alt="Notification Image" className="img-thumbnail list-thumbnail xsmall border-0 rounded-circle"/>
                                </a>
                                <div className="pr-3 pl-2">
                                    <a href="#">
                                        <p className="font-weight-medium mb-1">1 مورد موجود نیست!</p>
                                        <p className="text-muted mb-0 text-small"><span className="numberltr">09.04.2018 - 12:45</span></p>
                                    </a>
                                </div>
                            </div>


                            <div className="d-flex flex-row mb-3 pb-3 border-bottom">
                                <a href="#">
                                    <img src="assets/img/notification-thumb-2.jpg" alt="Notification Image" className="img-thumbnail list-thumbnail xsmall border-0 rounded-circle"/>
                                </a>
                                <div className="pr-3 pl-2">
                                    <a href="#">
                                        <p className="font-weight-medium mb-1">سفارش جدید دریافت شد این مجموع 147،20 دلار است.</p>
                                        <p className="text-muted mb-0 text-small"><span className="numberltr">09.04.2018 - 12:45</span></p>
                                    </a>
                                </div>
                            </div>

                            <div className="d-flex flex-row mb-3 pb-3">
                                <a href="#">
                                    <img src="assets/img/notification-thumb-3.jpg" alt="Notification Image" className="img-thumbnail list-thumbnail xsmall border-0 rounded-circle"/>
                                </a>
                                <div className="pr-3 pl-2">
                                    <a href="#">
                                        <p className="font-weight-medium mb-1">3 مورد فقط توسط یک کاربر به لیست دلخواه اضافه شده است!</p>
                                        <p className="text-muted mb-0 text-small"><span className="numberltr">09.04.2018 - 12:45</span></p>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div> */}

                    <button className="header-icon btn btn-empty d-none d-sm-inline-block" type="button" id="fullScreenButton">
                        <i className="flaticon-fullscreen"></i>
                        <i className="flaticon-normalscreen"></i>
                    </button>

                </div>

            </div>
                <a href="#" className="menu-button d-none d-md-block">
                    <i className="glyph-icon flaticon-menu"></i>
                </a>

                <a href="#" className="menu-button-mobile d-xs-block d-sm-block d-md-none">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 17">
                        <rect x="0.5" y="0.5" width="25" height="1"/>
                        <rect x="0.5" y="7.5" width="25" height="1"/>
                        <rect x="0.5" y="15.5" width="25" height="1"/>
                    </svg>
                </a>
        </nav>
        );
    }
}

export {Header};
