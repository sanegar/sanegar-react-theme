import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
// import {loadThemeStyle, themeLoad} from './js';
import{Lang} from './../';

class Menu extends Component{
    constructor(props){
        super(props);
        this.state = {lastMenus: [], menuCategory: ""};
        this.checkActiveChild = this.checkActiveChild.bind(this);
        this.checkActiveParent = this.checkActiveParent.bind(this);
    }

    checkActiveParent(childs=[], category){
        // console.log(this.props.location);
        // console.log(this.props.location+" "+path);
        // let childs = rightChild[name];
        let className = "";
        childs.forEach(item => {
            if(item.method!= undefined)
                if("/"+category+item.method.react_path == this.props.path){
                    className = "active";
                }
        });

        return className;
    }

    componentDidMount(){
        // alert(12);
        window.runMenu();
        // window.activeMenu();
    }

    checkActiveChild(path){
        // console.log(this.props.location);
        // console.log(this.props.path +" "+ path);
        if(this.props.path == path)
            return "active"
        else
            return ""
    }

    shouldComponentUpdate(nextProps, nextState){
        // console.log(props);
        // alert(123);
        if(this.state.lastMenus.length == 0) return true;

        let items = this.props.path.split('/');
        let category = items[1];
        let menuItems = [];
        nextProps.menus.forEach((catItem)=>{
            if(catItem.name == category)
                menuItems = catItem.menus;
        });

        if(menuItems.length > this.state.lastMenus.length || category != this.state.menuCategory) return true;
        else return false;
    }

    render(){

        let {menus,path} = this.props;
        // console.log("menus");
        let items = path.split('/');
        let category = items[1];
        // alert(menus.length);
        var menuItems = [];
        menus.forEach((catItem)=>{
            if(catItem.name == category)
                menuItems = catItem.menus;
        });
        // console.log(menuItems);
        this.state.lastMenus = menuItems;
        this.state.menuCategory = category;

        return(
        <div className="sidebar">
            <div className="main-menu">
                <div className="scroll">
                    <ul className="list-unstyled">
                        {
                            menuItems.map((rmenu, index)=>
                                <li data-link={rmenu.name} key={index} className={this.checkActiveParent(rmenu.childs, category)}>
                                    <a>
                                        <i className={rmenu.icon_class}></i>
                                        <span> {Lang('public.'+rmenu.name)} </span>
                                    </a>
                                </li>
                            )
                        }
                    </ul>
                </div>
            </div>

            <div className="sub-menu">
                <div className="scroll ps">
                    {menuItems.map((item, index)=>
                        <ul className="list-unstyled" key={index} data-link={item.name}>
                            {(item.childs!=undefined?item.childs:[]).map((menu, i)=>{
                                if(menu.method != undefined)
                                {
                                    let path = "/"+category+menu.method.react_path;
                                    if(menu.method.react_path.substr(0, 1) == "@")
                                        path = menu.method.react_path.replace("@", "");

                                    return <li key={i} className={this.checkActiveChild(path)}>
                                        <Link to={path}>
                                            <i className={"fas fa-fw "+ menu.icon_class}></i>
                                            <span>{Lang('public.'+menu.name)}</span>
                                        </Link>
                                    </li>
                                }
                            })}
                        </ul>
                    )}
                </div>
            </div>
        </div>
        );
    }

    renderOld(){

        let {menus} = this.props;

        // console.log("menus: ");
        // console.log(menus);

        return(
        <div className="sidebar">
            <div className="main-menu">
                <div className="scroll">
                    <ul className="list-unstyled">
                        {
                            rightParent.map((rmenu, index)=>
                                <li data-link={rmenu.name} key={index} className={this.checkActiveParent(rmenu.name)}>
                                    <a>
                                        <i className={rmenu.icon}></i>
                                        <span>{Lang('public.'+rmenu.name)}</span>
                                    </a>
                                </li>
                            )
                        }
                    </ul>
                </div>
            </div>

            <div className="sub-menu">
                <div className="scroll">
                    {Object.keys(rightChild).map((attr, index)=>
                        <ul className="list-unstyled" key={index} data-link={attr}>
                            {rightChild[attr].map((menu, i)=>
                                <li key={i} className={this.checkActiveChild(menu.path)}>
                                    <Link to={menu.path}>
                                        <i className={"fas fa-fw "+ menu.icon}></i>
                                        <span>{Lang('public.'+menu.name)}</span>
                                    </Link>
                                </li>
                                )}
                        </ul>
                    )}
                </div>
            </div>
        </div>
        );
    }
}
const mapStateToProps = (state)=>({
    menus:state.menus
})
// export default connect(mapStateToProps)(Menu);
export default Menu;
