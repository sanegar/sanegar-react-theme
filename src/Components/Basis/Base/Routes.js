import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import BreadCrumb from './BreadCrumb';
// import {connect} from 'react-redux';
// import * as Components from './../../Components';

import {LogOut} from '../Base/LogOut';

class Routes extends Component{
    render(){
        let {routes, Components} = this.props;

        return(
            <main>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <Route path="/:path" component={BreadCrumb} />
                            <Route path="/exit" component={LogOut} />
                            {/* <div className="separator mb-5"></div> */}
                            {
                                routes.map((route, index)=> {
                                    if(route.namespace != undefined && route.component != undefined){
                                        if(typeof Components[route.namespace][route.component] == "function"){
                                            let Module = Components[route.namespace][route.component];
                                            let group = route.group;
                                            if(group == "@")
                                                group = "";
                                            else if(group != null && group.substr(0,1) != "/")
                                                group = "/"+group;
                                            let path = group+route.path;
                                            // console.log(index+": "+path);

                                            return <Route
                                                path={path}
                                                key={index}
                                                component={(props)=><Module {...props} />}
                                                // exact={route.exact=="true"?true:false} />
                                                exact={route.exact=="true"?true:false} />
                                        }
                                    }
                                })
                            }
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

// export default connect((state)=>({routes: state.routes}))(Routes);
export default Routes;
