import React, {Component} from 'react';

class LogOut extends Component{
    constructor(props){
        super(props);
    }

    componentDidMount(){
        // this.refs.logoutform.submit();
        // logout-form
        window.document.getElementById('logout-form').submit();
    }

    render(){
        // alert(123);
        let token = window.$('meta[name="csrf-token"]').attr('content');
        // console.log(window.$('meta[name="csrf-token"]').attr('content'));
        return(
        <React.Fragment>
            <div style={{textAlign:"center"}}>
                خروج از سیستم مدیریت
            </div>
            <form id="logout-form" action="/admin/logout" method="POST" style={{display: "none"}}>
                <input type="hidden" value={token} name="_token" />
            </form>
        </React.Fragment>);
    }
}

export {LogOut};
