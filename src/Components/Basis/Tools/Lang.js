// import {connect} from 'react-redux';
// import {store} from './../../redux/store';
import Config from './../../Config';

const Lang = (str) => {
    // let lang = store.getState().lang;
    let lang = Config.getStore().getState().lang;

    let param = str.split('.');

    if(Object.keys(lang).length > 0)
    return (lang[param[0]][param[1]]);
}

export default Lang;
