import React, {Component} from 'react';
import axios from 'axios';
import Config from '../../Config';
import {NotificationManager} from 'react-notifications';

export class CheckList extends Component {
    constructor(props){
        super(props);
        this.change = this.change.bind(this);
    }

    change(model,id,field){
        axios.get(`${Config.getHost()}/admin/changeStatus/${model}/${id}/${field}`)
            .then((response)=>{
                let rand = Math.random()*10000;
                    NotificationManager.success(response.data, 'تغییر وضعیت انجام شد.', 5000);
                    this.props.history.replace(this.props.history.location.pathname+`?${rand}`);
            })
            .catch((error)=>{
                console.log(error);
            });
    }

    render(){
        let {name, value, defaultValue, className ,error,model,id,field} = this.props;
        let rand = Math.random(1000, 9999);
        let divError = [];
        if(error!=undefined){
            error.map((err, index)=>{
                divError.push(<div className='alert alert-danger' id={'error-'+rand} key={index} style={{display:"block"}}>{err}</div>);
            })
        }
        // console.log('field');
        // console.log(field);
        return(<div className="form-group" >
                <div className="custom-switch custom-switch-secondary mb-2">
                    <input name={name} key={rand} ref='item' className="custom-switch-input"
                        id={"switch2"+rand} type="checkbox" onClick = {()=>this.change(model,id,field)} defaultChecked={defaultValue==1?true:false} value={1} />
                    <label className="custom-switch-btn" htmlFor={"switch2"+rand}  dir="ltr"></label>
                </div>
            </div>);
    }
}
