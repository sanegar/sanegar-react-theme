import React, {Component} from 'react';

export class PolarChart extends Component {
    constructor(props){
        super(props);
        this.state ={
            data : props.data!=undefined? props.data:[50,60,80],
            labels : props.labels!=undefined? props.labels:['فروشنده','بنکدار','تولیدکننده']
        }


    }

    componentDidMount(){
        Chart.defaults.PolarWithShadow = Chart.defaults.polarArea, Chart.controllers.PolarWithShadow = Chart.controllers.polarArea.extend({
            draw: function(e) {
                Chart.controllers.polarArea.prototype.draw.call(this, e);
                let t = this.chart.chart.ctx;
                t.save(), t.shadowColor = "rgba(0,0,0,0.2)", t.shadowBlur = 10, t.shadowOffsetX = 5, t.shadowOffsetY = 10, t.responsive = !0, Chart.controllers.polarArea.prototype.draw.apply(this, arguments), t.restore()
            }
        });


        if (document.getElementById("polarChart")) {
            var M = document.getElementById("polarChart").getContext("2d");
            new Chart(M, {
                type: "PolarWithShadow",
                options: {
                    plugins: {
                        datalabels: {
                            display: !1
                        }
                    },
                    responsive: !0,
                    maintainAspectRatio: !1,
                    scale: {
                        ticks: {
                            display: !1
                        }
                    },
                    legend: {
                        position: "bottom",
                        labels: {
                            padding: 30,
                            usePointStyle: !0,
                            fontSize: 10
                        }
                    }
                    // ,
                    // tooltips: S
                },
                data: {
                    datasets: [{
                        label: "موجودی",
                        borderWidth: 2,
                        pointBackgroundColor: 'rgba(0,0,0,0.15)',
                        borderColor: ['red', 'blue','green'],
                        backgroundColor: ['rgba(0,0,0,0.15)', 'rgba(0,0,0,0.15)','rgba(0,0,0,0.15)'],
                        data:this.state.data
                    }],
                    labels: this.state.labels
                }
            })
        }
    }

    componentWillReceiveProps(props){
        // console.log(props);
        this.setState({
            data: props.data!=undefined? props.data:[],
            labels: props.labels!=undefined? props.labels:[],
        });
        // console.log(this.state.data);
    }

    render(){
        // let {items} = this.state;
        // console.log(this.state.data);

        // console.log(items);
        return(<div className="col-md-6 col-sm-12 mb-4">
                    <div className="card h-100">
                        <div className="card-body">
                            <h5 className="card-title">نمودار دایره ای</h5>
                            <div className="dashboard-donut-chart">
                                <canvas id="polarChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>);
    }
}
