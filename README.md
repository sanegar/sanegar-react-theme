# react-theme

> included saba and modiran template

[![NPM](https://img.shields.io/npm/v/react-theme.svg)](https://www.npmjs.com/package/sanegar-react-theme) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save sanegar-react-theme
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'react-theme'

class Example extends Component {
  render () {
    return (
      <MyComponent />
    )
  }
}
```

## License

MIT © [mw313](https://github.com/mw313)
